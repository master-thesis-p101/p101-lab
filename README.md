# P101Lab

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 18.x.

## Build

Install a suitable version of NPM (this project was confirmed to work with v16.15.1, which you can obtain using [`nvm`](https://github.com/nvm-sh/nvm)), then run `npm ci` and `npm run build`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build webapp

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Deploying

The files in the `dist` folder can be deployed to a web server as static files.

## Build and package native apps

In addition to deploying the webapp to a web server, one can generate native apps using [electron-forge](https://www.electronforge.io/). Just run

    npm run make -- --platform win32

You can replace `win32` with `linux` (to generate a `.deb`) or `darwin` (for Mac OS).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
