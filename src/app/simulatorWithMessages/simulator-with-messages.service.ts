import { Injectable } from '@angular/core';
import { SimulatorService } from '@app/simulator/simulator.service';
import { MessageService } from '@app/message';

@Injectable({
  providedIn: 'root'
})
export class SimulatorWithMessagesService extends SimulatorService {

  constructor( private messages: MessageService ) {
    super ();
    if (typeof Worker === 'undefined') {
      this.messages.showMessage ("Questo browser non supporta i Web Worker: la pagina si impallerà se esegui un programma che ci mette troppo tempo a terminare", "warning");
    }
    this.error$.subscribe (e => {
      if (e!=null) {
        this.messages.showMessage (e, "error");
      }
    });
  }

  abortProgram () {
    const f = super.abortProgram ();
    if (f) {
      this.messages.showMessage ("Programma interrotto", "warning");
    }
    return f;
  }
}
