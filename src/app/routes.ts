
import { Routes, Route } from '@angular/router';

import { CreditsComponent } from './gui/credits/credits.component';
import { EditorComponent } from './gui/editor/editor.component';
import { SimulatorComponent } from './gui/simulator/simulator.component';

export interface RouteWithLabel extends Route {
  label?: string;
}

export type RoutesWithLabels = RouteWithLabel[]; 

export const routes: RoutesWithLabels = [
    { path: 'simulator', component: SimulatorComponent, label: 'Simulatore' },
    { path: 'editor', component: EditorComponent, label: 'Editor' },
    { path: 'credits', component: CreditsComponent, label: 'Info' },
    { path: '**', redirectTo: '/simulator', pathMatch: 'full' },
  ]
