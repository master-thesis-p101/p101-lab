import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MessageKind } from './models';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private _snackBar: MatSnackBar) { }

  showMessage (message: string, level: MessageKind = 'info') {
    if (level == "error") message = "ERRORE: " + message
    this._snackBar.open (message, '', {
      duration: 3000,
      panelClass: level,
    })
  }
}
