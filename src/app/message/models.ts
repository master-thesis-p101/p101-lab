
export type MessageKind = 'info' | 'success' | 'error' | 'warning';
