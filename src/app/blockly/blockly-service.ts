import { Injectable } from '@angular/core';

import { blocks_definitions } from './blockly-configurations';
import * as Blockly from 'blockly';


@Injectable({
  providedIn: 'root'
})
export class BlocklyService {

  constructor() { 
      Blockly.defineBlocksWithJsonArray (blocks_definitions);
  }

}
