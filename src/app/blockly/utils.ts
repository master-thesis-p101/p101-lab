
import {decode_constant, Instruction, isConstantAsInstruction, isHalfRegister, isJumpInstruction} from '@app/simulator';

const operator_to_block_type = {
    '+': "somma",
    '-': "sottrai",
    'x': "moltiplica",
    ':': "dividi",
    'sqrt': "sqrt",
    'fromM': "fromM",
    'toA': "toA",
    'exchange': "exchange",
    '*': "*",
    'print': "print",
}

function translate_block_type_and_fields (i: Instruction) {
    // fixed code
    if (i.operator=='S') return 'type="S">';
    if (i.register == '/' && i.operator=='exchange') return 'type="decimalA">';
    if (i.register=='A' && i.operator=='exchange') return 'type="abs">';
    if (i.register=='/' && i.operator=='print') return 'type="interline">';
    // jump instructions
    if (isJumpInstruction (i)) {
        const r = i.register;
        let type: string, a: number, b: number;
        if (r=='' || r == 'M' || r=='C' || r=='D' || r=='R' ) type = 'unconditionalJump';
        else if (r=='A' || r=='B' || r=='E' || r=='F' ) type = 'setLabel';
        else if (r=='/' || r=='c' || r=='d' || r=='r' ) type = 'conditionalJump';
        else if (r=='a' || r=='b' || r=='e' || r=='f' ) type = 'setConditionalLabel';
        else throw new Error ("(BUG) register in jumpInstruction must be one of the jump registers, got "+r);
        if (r == '' || r == 'M' || r== 'A' || r == '/' || r== 'a') a=0;
        else if (r == 'B' || r== 'C' || r == 'b' || r== 'c') a=1;
        else if (r == 'E' || r== 'D' || r == 'e' || r== 'd') a=2;
        else if (r == 'F' || r== 'R' || r == 'f' || r== 'r') a=3;
        else throw new Error ("(BUG) register in jumpInstruction must be one of the jump registers, got" + r);
        if (i.operator == 'V') b=1;
        else if (i.operator == 'W') b=2;
        else if (i.operator == 'Y') b=3;
        else if (i.operator == 'Z') b=4;
        else throw new Error ("(BUG) operator in jumpInstruction must be one of V,W,Y,Z. Got" + i.operator);
        const fakelabel = (r == '/' || r=='r' || isHalfRegister (r)) ? 'C' + (4*a+b) : '' + (4*a+b);
        return 'type="' + type + '"><field name="fakelabel">'+ fakelabel +'</field>';
    }
    // register can be chosen
    else {
        const t = operator_to_block_type[i.operator];
        const r = i.register || "M";
        return 'type="' + t + '"><field name="registro">'+ r +'</field>';
    } 
}

export function instructions_to_xml_str (instructions: Instruction[]) {
    let out = "<xml>\n";
    let n_blocks = 0;
    for (let j=0; j<instructions.length; ++j) {
        const inst = instructions[j];
        const prefix = (j==0) ? '          <block id="0" x="110" y="86" ' : '    <next><block id="'+j+'" ';
        if (isConstantAsInstruction (inst)) {
            const {constant, index} = decode_constant (instructions, j);
            out += prefix + 'type="constantInM"><field name="constant">'+constant.toString()+'</field>\n';
            if (index <= j) throw new Error ("(BUG) decode_constant did not advance the instruction index. First coding instruction index="+j);
            j = index;
            ++n_blocks;
        }
        else if (!isJumpInstruction(inst) && isHalfRegister(inst.register)) {
            throw new Error ("translating programs with half registers is not yet implemented");
        }
        else {
            out += prefix + translate_block_type_and_fields (inst) + '\n';
            ++n_blocks;
        }
    }
    if (n_blocks > 0) {
        out += "    ";
        for (let j=0; j<n_blocks-1; ++j) {
            out += "</block></next>";
        }
    }
    if (instructions.length >= 1) {
        out += '\n        </block>\n';
    }
    out += "</xml>\n";
    console.debug ("[BlocklyUtils] translate program", instructions, "output xml", out);
    return out;
}
