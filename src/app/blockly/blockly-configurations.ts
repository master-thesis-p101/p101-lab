import { encode_constant, halfRegistersMap, Label, Operator, Register } from "@app/simulator";
import * as Blockly from "blockly";
import {Big} from 'big.js';

export const blocks_definitions = [{
    "type": "somma",
    "message0": "somma %1",
    "args0": [
        {
        "type": "field_dropdown",
        "name": "registro",
        "options": [["M", "M"],["A","A"],["R","R"],["B","B"],["C","C"],["D","D"],["E","E"],["F","F"]]
        }
    ],
    "previousStatement": null,
    "nextStatement": null,
    "colour": "#666259",
    "tooltip": "somma il valore del registro selezionato ad A",
    "helpUrl": ""
    },
    {
        "type": "sottrai",
        "message0": "sottrai %1",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "registro",
            "options": [["M", "M"],["A","A"],["R","R"],["B","B"],["C","C"],["D","D"],["E","E"],["F","F"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": "#666259",
        "tooltip": "sottrai il valore del registro selezionato da A",
        "helpUrl": ""
    },
    {
        "type": "moltiplica",
        "message0": "moltiplica per %1",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "registro",
            "options": [["M", "M"],["A","A"],["R","R"],["B","B"],["C","C"],["D","D"],["E","E"],["F","F"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": "#666259",
        "tooltip": "moltiplica A per il valore del registro selezionato",
        "helpUrl": ""
    },
    {
        "type": "dividi",
        "message0": "dividi per %1",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "registro",
            "options": [["M", "M"],["A","A"],["R","R"],["B","B"],["C","C"],["D","D"],["E","E"],["F","F"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": "#666259",
        "tooltip": "dividi A per il valore del registro selezionato",
        "helpUrl": ""
    },
    {
        "type": "sqrt",
        "message0": "radice di %1",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "registro",
            "options": [["M", "M"],["A","A"],["R","R"],["B","B"],["C","C"],["D","D"],["E","E"],["F","F"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": "#666259",
        "tooltip": "metti in A la radice del registro selezionato",
        "helpUrl": ""
    },
    {
        "type": "abs",
        "message0": "calcola valore assoluto di A",
        "previousStatement": null,
        "nextStatement": null,
        "colour": "#666259",
        "tooltip": "metti in A il valore assoluto di A",
        "helpUrl": ""
    },
    {
        "type": "decimalA",
        "message0": "calcola parte decimale di A",
        "previousStatement": null,
        "nextStatement": null,
        "colour": "666259",
        "tooltip": "metti in M la parte decimale di A",
        "helpUrl": ""
    },
    {
        "type": "constantInM",
        "message0": "metti %1 in M",
        "args0": [
          {
            "type": "field_number",
            "name": "constant",
            "value": 3.14
          }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 65,
        "tooltip": "metti in M la costante scelta",
        "helpUrl": ""
    },

    {
        "type": "fromM",
        "message0": "copia M in %1",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "registro",
            "options": [["B","B"],["C","C"],["D","D"],["E","E"],["F","F"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 210,
        "tooltip": "copia il valore di M nel registro selezionato",
        "helpUrl": ""
    },
    {
        "type": "toA",
        "message0": "copia %1 in A",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "registro",
            "options": [["M","M"],["R","R"],["B","B"],["C","C"],["D","D"],["E","E"],["F","F"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 210,
        "tooltip": "copia il valore del registro selezionato in A",
        "helpUrl": ""
    },
    {
        "type": "exchange",
        "message0": "scambia A con %1",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "registro",
            "options": [["M", "M"],["B","B"],["C","C"],["D","D"],["E","E"],["F","F"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 210,
        "tooltip": "scambia il valore di A con quello del registro selezionato",
        "helpUrl": ""
    },

    {
        "type": "S",
        "message0": "leggi un numero",
        "previousStatement": null,
        "nextStatement": null,
        "colour": 30,
        "tooltip": "ferma il programma in attesa di input da tastiera",
        "helpUrl": ""
    },
    {
        "type": "*",
        "message0": "azzera %1",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "registro",
            "options": [["A", "A"],["B","B"],["C","C"],["D","D"],["E","E"],["F","F"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 30,
        "tooltip": "azzera il registro selezionato",
        "helpUrl": ""
    },
    {
        "type": "print",
        "message0": "stampa %1",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "registro",
            "options": [["M", "M"],["A", "A"],["R", "R"],["B","B"],["C","C"],["D","D"],["E","E"],["F","F"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 30,
        "tooltip": "stampa il valore del registro selezionato",
        "helpUrl": ""
    },
    {
        "type": "interline",
        "message0": "stampa interlinea",
        "previousStatement": null,
        "nextStatement": null,
        "colour": 30,
        "tooltip": "stampa una linea vuota",
        "helpUrl": ""
    },

    {
        "type": "setLabel",
        "message0": "questo è il punto %1",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "fakelabel",
            "options": [["1", "1"],["2", "2"],["3", "3"],["4","4"],["5","5"],["6","6"],["7","7"],["8","8"],["9","9"],["10","10"],["11", "11"],["12", "12"],["13", "13"],["14","14"],["15","15"],["16","16"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 240,
        "tooltip": "definisce un punto in cui saltare con un'istruzione di salto incondizionato",
        "helpUrl": ""
    },
    {
        "type": "setConditionalLabel",
        "message0": "questo è il punto %1",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "fakelabel",
            "options": [["C1", "C1"],["C2", "C2"],["C3", "C3"],["C4","C4"],["C5","C5"],["C6","C6"],["C7","C7"],["C8","C8"],["C9","C9"],["C10","C10"],["C11", "C11"],["C12", "C12"],["C13", "C13"],["C14","C14"],["C15","C15"],["C16","C16"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 240,
        "tooltip": "definisce un punto in cui saltare con un'istruzione di salto condizionato",
        "helpUrl": ""
    },
    {
        "type": "unconditionalJump",
        "message0": "salta al punto %1",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "fakelabel",
            "options": [["1", "1"],["2", "2"],["3", "3"],["4","4"],["5","5"],["6","6"],["7","7"],["8","8"],["9","9"],["10","10"],["11", "11"],["12", "12"],["13", "13"],["14","14"],["15","15"],["16","16"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 240,
        "tooltip": "l'esecuzione riprende dal punto selezionato",
        "helpUrl": ""
    },
    {
        "type": "conditionalJump",
        "message0": "se A>0 salta al punto %1",
        "args0": [
            {
            "type": "field_dropdown",
            "name": "fakelabel",
            "options": [["C1", "C1"],["C2", "C2"],["C3", "C3"],["C4","C4"],["C5","C5"],["C6","C6"],["C7","C7"],["C8","C8"],["C9","C9"],["C10","C10"],["C11","C11"],["C12", "C12"],["C13", "C13"],["C14","C14"],["C15","C15"],["C16","C16"]]
            }
        ],
        "previousStatement": null,
        "nextStatement": null,
        "colour": 240,
        "tooltip": "se il valore di A è positivo l'esecuzione riprende dal punto selezionato, altrimenti l'esecuzione continua normalmente",
        "helpUrl": ""
    },

];
 
const toolbox_definition: any = {
  "kind": "flyoutToolbox",
  "contents": [
        {
          "kind": "block",
          "type": "somma"
        },
        {
          "kind": "block",
          "type": "sottrai"
        },
        {
          "kind": "block",
          "type": "moltiplica"
        },
        {
          "kind": "block",
          "type": "dividi"
        },
        {
          "kind": "block",
          "type": "abs"
        },
        {
          "kind": "block",
          "type": "sqrt"
        },
        {
          "kind": "block",
          "type": "decimalA"
        },
        {
          "kind": "block",
          "type": "constantInM"
        },
        
        {
          "kind": "block",
          "type": "fromM"
        },
        {
          "kind": "block",
          "type": "toA"
        },
        {
          "kind": "block",
          "type": "exchange"
        },
  
        {
          "kind": "block",
          "type": "S"
        },
        {
          "kind": "block",
          "type": "*"
        },
        {
          "kind": "block",
          "type": "print"
        },
        {
          "kind": "block",
          "type": "interline"
        },
  
        {
          "kind": "block",
          "type": "setLabel"
        },
        {
          "kind": "block",
          "type": "unconditionalJump"
        },
        {
          "kind": "block",
          "type": "setConditionalLabel"
        },
        {
          "kind": "block",
          "type": "conditionalJump"
        },
      ]
};

export const blockly_options: any = {
  toolbox: toolbox_definition,
  scrollbars: true,

}

// CODE GENERATION: instruction for which the register can be chosen

const blockname_to_operator_map: {[bname: string]: Operator} = {
  "somma": '+',
  "sottrai": '-',
  "moltiplica": 'x',
  "dividi": ':',
  "sqrt": 'sqrt',
  "fromM": 'fromM',
  "toA": 'toA',
  "exchange": 'exchange',
  "*": '*',
  "print": 'print',  
};

Object.keys (blockname_to_operator_map).forEach ( block_name => {
  (Blockly as any).JavaScript[block_name] = (block: Blockly.Block) => {
    const registro = block.getFieldValue('registro');
    const operator = blockname_to_operator_map[block_name];
    return registro + ',' + operator + ' ' + block.id + '\n';
  };
});

// CODE GENERATION: instruction for which the code is fixed

const blockname_to_constant_code_map: {[bname: string]: string} = {
  "abs": 'A,exchange',
  "decimalA": '/,exchange',
  "S": ',S',
  "interline": '/,print',
};

Object.keys (blockname_to_constant_code_map).forEach ( block_name => {
  (Blockly as any).JavaScript[block_name] = (block: Blockly.Block) => {
    return blockname_to_constant_code_map[block_name]+ ' ' + block.id+"\n";
  };
});

function fakelabel_to_source_register (fakelabel: number): Register {
  const div = Math.floor((fakelabel-1)/4);
  const fakelabel_map: Register[] = ['', 'C', 'D', 'R'];
  return fakelabel_map[div];
}
function fakelabel_to_destination_register (fakelabel: number): Register {
  const div = Math.floor((fakelabel-1)/4);
  const fakelabel_map: Register[] = ['A', 'B', 'E', 'F'];
  return fakelabel_map[div];
}
function fakelabel_to_label (fakelabel: number): Label {
  const mod = fakelabel%4;
  const fakelabel_map: Label[] = ['Z', 'V', 'W', 'Y'];
  return fakelabel_map[mod];
}

function translate_jump_instruction (block: Blockly.Block) {
  const fakelabel: string = block.getFieldValue('fakelabel');
  let n: number, conditional: boolean;
  if (fakelabel.startsWith ("C")) {
    n = +fakelabel.substring(1);
    conditional = true;
  }
  else {
    n = +fakelabel;
    conditional = false;
  }
  let src_reg = fakelabel_to_source_register (n);
  let dest_reg = fakelabel_to_destination_register (n);
  const label = fakelabel_to_label (n);
  if (conditional) {
    src_reg = halfRegistersMap[src_reg];
    dest_reg = halfRegistersMap[dest_reg];
  }
  if (block.type.startsWith("set")) {
    return dest_reg + ',' + label+ ' ' + block.id+"\n";
  }
  else {
    return src_reg + ',' + label+ ' ' + block.id+"\n";
  }
};

(Blockly as any).JavaScript["setLabel"] = translate_jump_instruction;
(Blockly as any).JavaScript["setConditionalLabel"] = translate_jump_instruction;
(Blockly as any).JavaScript["unconditionalJump"] = translate_jump_instruction;
(Blockly as any).JavaScript["conditionalJump"] = translate_jump_instruction;

(Blockly as any).JavaScript["constantInM"] = (block: Blockly.Block) => {
  const n: number = block.getFieldValue('constant') || new Big(0);
  const coding_instructions = encode_constant ( new Big (n) );
  return coding_instructions.map (i => i.register + ',' + i.operator+ ' ' + block.id ).reduce ( (a,b) => a+"\n"+b )+"\n";
}
