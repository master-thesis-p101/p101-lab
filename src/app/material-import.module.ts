
import { NgModule } from '@angular/core';

import {MatExpansionModule} from '@angular/material/expansion';
import {MatFormFieldModule} from '@angular/material/form-field'; 
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatGridListModule} from '@angular/material/grid-list'; 
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTabsModule} from '@angular/material/tabs'; 
import {MatMenuModule} from '@angular/material/menu'; 
import {MatDialogModule} from '@angular/material/dialog'; 
import {MatListModule} from '@angular/material/list'; 
import {MatSnackBarModule} from '@angular/material/snack-bar';

@NgModule({
    exports: [
      MatExpansionModule,
      MatFormFieldModule,
      MatInputModule,
      MatButtonModule,
      MatGridListModule,
      MatButtonToggleModule,
      MatIconModule,
      MatTooltipModule,
      MatTabsModule,
      MatMenuModule,
      MatDialogModule,
      MatListModule,
      MatSnackBarModule,      
    ]
})
export class MaterialImportModule {}
