import { Component } from '@angular/core';
import { routes, RoutesWithLabels } from '@app/routes';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  navLinks: RoutesWithLabels;

  constructor () {
    this.navLinks = routes.filter (r => r.label);
  }

}
