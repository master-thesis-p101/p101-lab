
import {Instruction} from '@app/simulator';

export type Program = {
    instructions: Instruction[];
    title: string;
};
