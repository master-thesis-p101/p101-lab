import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Instruction } from '@app/simulator';
import { BehaviorSubject, Subject } from 'rxjs';
import { startWith } from 'rxjs/operators';
import { Program } from './models';

const LOCAL_STORAGE_PROGRAMS_KEY = 'programs_v_0.4';

@Injectable({
  providedIn: 'root'
})
export class MagneticCardsHolderService {

  private _programs: Program[] = [];
  private _programs$ = new Subject <Program[]> ();
  private _selected_card$ = new BehaviorSubject <Program | null> (null);
  private _loaded_card$ = new Subject <Program> ();
  private _edited_card_id$ = new Subject <number> ();

  constructor(private router: Router) { 
    this.load_programs_from_localstorage ();
  }

  private update_programs_in_localstorage () {
    localStorage.setItem (LOCAL_STORAGE_PROGRAMS_KEY, JSON.stringify(this._programs));
  }

  private load_programs_from_localstorage () {
    const str_programs = localStorage.getItem (LOCAL_STORAGE_PROGRAMS_KEY);
    if (!str_programs) {
      this._programs = [];
    }
    else {
      this._programs = JSON.parse (str_programs);
    }
  }

  addProgram ( p: Program ) {
    this._programs.unshift (p);
    this.update_programs_in_localstorage ();
    this._programs$.next (this._programs);
    this.setSelectedCard (null);
    return 0;
  }

  updateProgram (id: number, code: Instruction[] | null, title?: string) {
    console.debug ("[Magnetic-cards-holder-service] update card", id, "code", code, "title", title);
    if (id >=0 && id < this._programs.length) {
      const program_to_update = this._programs[id];
      if (code != null) program_to_update.instructions = code;
      if (title != null) program_to_update.title = title;
      this.update_programs_in_localstorage ();
      this._programs$.next (this._programs);
    }
  }

  setSelectedCard ( id: number | null ) {
    if (id != null && id >= 0 && id < this._programs.length) {
      this._selected_card$.next (this._programs[id]);
    }
    else {
      this._selected_card$.next (null);
    }
  }
  
  editCard ( id: number | null ) {
    const params: {} = {};
    if (id != null && id >= 0 && id < this._programs.length) {
      this._edited_card_id$.next (id);
      params["id_card"] = id;
    }
    this.router.navigate (["editor"], {queryParams: params});
  }
  
  getProgram ( id: number ) {
    if (id != null && id >= 0 && id < this._programs.length) {
      return this._programs[id];
    }
    return null;
  }

  deleteCard (id: number) {
    // console.debug ("[Magnetic-cards-holder-service] deleting card", id);
    if (id >=0 && id < this._programs.length) {
      this._programs.splice (id, 1);
      this.update_programs_in_localstorage ();
      this._programs$.next (this._programs);
    }
  }

  loadProgram (id: number) {
    const params: {} = {};
    if (id >=0 && id < this._programs.length) {
      this._loaded_card$.next (this._programs[id]);
      params["id_card"] = id;
    }
    this.router.navigate (["simulator"], {queryParams: params});
  }

  get programs$ () {
    return this._programs$.asObservable().pipe (
      startWith (this._programs)
    );
  }

  get selectedCard$ () {
    return this._selected_card$.asObservable();
  }

  get loadedCard$ () {
    return this._loaded_card$.asObservable();
  }
  
  get editedCardId$ () {
    return this._edited_card_id$.asObservable();
  }

}
