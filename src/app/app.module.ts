import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { SimulatorComponent } from './gui/simulator/simulator.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RegistersComponent } from './gui/registers/registers.component';

import { RouterModule } from '@angular/router';

import { MaterialImportModule } from './material-import.module';
import { Programma101Component } from './gui/programma101/programma101.component';
import { MagneticCardComponent } from './gui/magnetic-card/magnetic-card.component';
import { MagneticCardsHolderComponent } from './gui/magnetic-cards-holder/magnetic-cards-holder.component';

import { routes } from './routes';
import { EditorComponent } from './gui/editor/editor.component';
import { BlocklyComponent } from './gui/blockly/blockly.component';
import { SaveAsDialogComponent } from './gui/save-as-dialog/save-as-dialog.component';
import { NumericKeyboardComponent } from './gui/blockly/numeric-keyboard/numeric-keyboard.component';
import { CreditsComponent } from './gui/credits/credits.component';

// TODO: remove the CommonJS version of 'big.js' from the dependency, use ES module when it will eventually came out.
//       remove the CommonJS version of 'blockly' from the dependency, use ES module when it will eventually came out.
//       then remove the whitelisted CommonJs dependencies in angular.json

@NgModule({
  declarations: [
    AppComponent,
    SimulatorComponent,
    RegistersComponent,
    Programma101Component,
    MagneticCardComponent,
    MagneticCardsHolderComponent,
    EditorComponent,
    BlocklyComponent,
    SaveAsDialogComponent,
    NumericKeyboardComponent,
    CreditsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialImportModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
