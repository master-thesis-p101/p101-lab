import { Component, OnDestroy, OnInit } from '@angular/core';

import { SimulatorWithMessagesService } from '@app/simulatorWithMessages/simulator-with-messages.service';
import { MagneticCardsHolderService } from '@app/magnetic-cards-holder/magnetic-cards-holder.service';
import { Observable, Subscription } from 'rxjs';
import { Program } from '@app/magnetic-cards-holder';

// TODO: call cards_service.updateProgram when the title of the inserted card changes

@Component({
  selector: 'app-programma101',
  templateUrl: './programma101.component.html',
  styleUrls: ['./programma101.component.scss']
})
export class Programma101Component implements OnInit, OnDestroy {

  private _subscription = new Subscription();

  print_pr_on = false;
  record_pr_on = false;

  program_title = "";
  card_inserted = false;
  selected_card$: Observable <Program | null>;
  selected_card: Program | null;

  constructor( public sim_service: SimulatorWithMessagesService, public cards_service: MagneticCardsHolderService ) { }

  ngOnInit(): void {

    this.selected_card$ = this.cards_service.selectedCard$;
    const s1 = this.cards_service.loadedCard$.subscribe ( c => {
      this.card_inserted = true;
      this.program_title = c.title;
      this.sim_service.program_code = c.instructions;
    });
    this._subscription.add (s1);

    const s2 = this.selected_card$.subscribe (c => {
      if (!this.record_pr_on) {
        this.selected_card = c;
      }
    });
    this._subscription.add (s2);

    const s3 = this.sim_service.reset$.subscribe ( _ => {
      this.record_pr_on = this.card_inserted = this.print_pr_on = false;
      this.program_title = "";
      this.selected_card = null;
    });
    this._subscription.add (s3);

  }

  handleKeydown ($event: KeyboardEvent) {
    let c = $event.key.toUpperCase();
    switch (c) {
      // handle invalid selectors
      case '.': 
        c = "dot";
        break;
      case '/':
        c = "slash";
        break;
      case ',':
        c = "comma";
        break;
      case '*':
        c = "times";
        break;
      case '-':
        c = "minus";
        break;
      case '+':
        c = "plus";
        break;
    }
    const target = ($event.target as HTMLElement);
    // console.debug ("[P101Component] target is", target);
    const associated_key = target.querySelector ("#key-"+c) as HTMLElement;
    if (!$event.altKey && !$event.ctrlKey && associated_key) {
      $event.preventDefault ();
      // console.log ("associated key is", associated_key);
      associated_key.classList.add ("pressed");
      associated_key.click ();
      setTimeout (() => associated_key.classList.remove("pressed"), 100);
    }
  }

  print_pr_toggled() {
    this.print_pr_on = !this.print_pr_on;
    this.sim_service.printPrToggled ();
  }
  
  record_pr_toggled() {
    this.record_pr_on = !this.record_pr_on;
    this.card_inserted = this.record_pr_on;
    this.sim_service.recordPrToggled ();
    if (this.record_pr_on) {
      this.program_title = "";
      this.selected_card = null;
    }
    else {
      this.cards_service.addProgram ({
        instructions: this.sim_service.program_code,
        title: this.program_title
      });
    }
  }

  card_dropped ($event: DragEvent) {
    $event.preventDefault ();
    console.debug ("[P101Component] dropped card", $event.dataTransfer?.getData("text/plain"));
    if (this.selected_card != null && !this.record_pr_on) {
      this.sim_service.program_code = this.selected_card.instructions;
      this.program_title = this.selected_card.title;
      this.card_inserted = true;
    }
  }

  allow_drop ($event: DragEvent) {
    if ( !this.record_pr_on ) {
      $event.preventDefault ();
    }
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe ();
    // reset assets when destroying component
    if (this.print_pr_on) this.sim_service.printPrToggled ();
    if (this.record_pr_on) this.sim_service.recordPrToggled ();
    if (this.sim_service.running) this.sim_service.abortProgram ();
  }


}
