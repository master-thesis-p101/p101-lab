import { Component, OnDestroy, OnInit, QueryList, ViewChildren } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';
import { MagneticCardsHolderService } from '@app/magnetic-cards-holder/magnetic-cards-holder.service';
import { Instruction, Label } from '@app/simulator';
import { SimulatorWithMessagesService } from '@app/simulatorWithMessages/simulator-with-messages.service';
import { MatDialog } from '@angular/material/dialog'; 
import { ChosenCard, SaveAsDialogComponent } from '@app/gui/save-as-dialog/save-as-dialog.component';
import { ActivatedRoute } from '@angular/router';
import { merge, Observable, Subscription } from 'rxjs';
import { debounceTime, filter, map, tap } from 'rxjs/operators';
import { Program } from '@app/magnetic-cards-holder';
import { MessageService } from '@app/message';

// TODO: this component is overcomplicated because mat-menus cannot be opened on hover. Fix it if they will ever implement this feature

type MenuKind = 'save' | 'run' | 'step';
const _DISABLED_REASON_MULTIPLE_PILES_SAVE = "ci sono più pile di blocchi, seleziona quella che vuoi salvare";
const _DISABLED_REASON_MULTIPLE_PILES_EXEC = "ci sono più pile di blocchi, seleziona quella che vuoi eseguire";
const _DISABLED_REASON_ALREADY_EXECUTION = "il programma è già in esecuzione";
const _DISABLED_REASON_NO_CODE = "il programma è vuoto";

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit, OnDestroy {

  @ViewChildren(MatMenuTrigger) triggers: QueryList<MatMenuTrigger>;
  private _program: Instruction[] | null;
  private _watch_router_params = true;
  private _first_emitted_program = true;
  private _subscription = new Subscription ();

  programToBeEdited$: Observable<Instruction[]>;
  programTitle = "";
  programId: number | null = null;

  dialog_opened = false;

  // 0: menu closed, 1: menu opened, 2: menu opened with focus
  private _menu_statuses = {
    'save': 0,
    'run': 0,
    'step': 0,
  }
  program_has_fakelabel = {1: false, 2: false, 3: false, 4: false};
  private _program_has_at_least_one_fakelabel = false;

  run_disabled: string | null = _DISABLED_REASON_NO_CODE;
  save_disabled: string | null = _DISABLED_REASON_NO_CODE;
  step_disabled: string | null = _DISABLED_REASON_NO_CODE;
  running = false;

  constructor (
    public sim_service: SimulatorWithMessagesService,
    public cards_service: MagneticCardsHolderService,
    public dialog_service: MatDialog,
    public routes: ActivatedRoute,
    private messages: MessageService,
  ) {}

  ngOnInit(): void {

    this.programToBeEdited$ = merge (
      this.cards_service.editedCardId$,
      this.routes.queryParams.pipe (
        filter (m => {
          const id = m["id_card"];
          return id != null && !isNaN (+id);
        }),
        map (m => +m["id_card"]),
        filter (_ => this._watch_router_params),
      )
    ).pipe (
      debounceTime (200),
      map (id => [id, this.cards_service.getProgram (id)]),
      filter (l => l[1]!=null),
      tap (([id, p]: [number, Program]) => {
        this.programId = id;
        this._watch_router_params = false;
        this.programTitle = p.title;
      }),
      map (([id, p]: [number, Program]) => p.instructions)
    );

    const s1 = this.sim_service.reset$.subscribe ( r => {
      if (r && this._program != null) {
        this.sim_service.program_code = this._program;
      }
    });

    const s2 = this.sim_service.executionStopped$.subscribe ( s => {
      this.step_disabled = null;
      this.running = false;
      console.log ("[EditorComponent] execution stopped emitted step disabled?", this.step_disabled);
    });
    
    const s3 = this.sim_service.executionStarted$.subscribe ( s => {
      if (s) {
        this.step_disabled = _DISABLED_REASON_ALREADY_EXECUTION;
        this.running = true;
        console.log ("[EditorComponent] execution started emitted step disabled?", this.step_disabled);
      }
    });

    this._subscription.add (s1);
    this._subscription.add (s2);
    this._subscription.add (s3);
  }

  programChanged ( program: Instruction[] | null ) {
    console.debug ("[EditorComponent] program changed. New length:", program ? program.length : null);
    this._program = program;
    this._detect_program_fakelabels();
    if (program != null) {
      if ( !this._first_emitted_program || this.sim_service.program_code.length == 0) {
        this.sim_service.program_code = program;
      }
      if (program.length == 0) {
        this.run_disabled = _DISABLED_REASON_NO_CODE;
        this.step_disabled = _DISABLED_REASON_NO_CODE;
        this.save_disabled = _DISABLED_REASON_NO_CODE;
      }
      else {
        this.run_disabled = null;
        this.step_disabled = null;
        this.save_disabled = null;  
      }
    }
    else {
      this.sim_service.program_code = [];
      this.run_disabled = _DISABLED_REASON_MULTIPLE_PILES_EXEC;
      this.step_disabled = _DISABLED_REASON_MULTIPLE_PILES_EXEC;
      this.save_disabled = _DISABLED_REASON_MULTIPLE_PILES_SAVE;
    }
    this._first_emitted_program = false;

  }

  save (trigger: MatMenuTrigger) {
    trigger.closeMenu ();
    console.debug ("[EditorComponent] save program", this._program);
    if (this._program) {
      if (this.programId == null) {
        this.save_other_card ();
      }
      else {
        this.cards_service.updateProgram (this.programId, this._program);
        this.messages.showMessage ("Programma salvato sulla scheda " + this.programTitle, "success");
      }
    }
  }

  save_other_card() {
    console.debug ("[EditorComponent] save in another card program", this._program);
    if (this._program != null) {
      this.dialog_opened = true;
      const dialog_ref = this.dialog_service.open (SaveAsDialogComponent, {width: '500px'});
      dialog_ref.afterClosed ().subscribe ( (choice: ChosenCard | undefined) => {
        this.dialog_opened = false;
        if (choice && choice.kind == "existing") {
          if (choice.existingId == null) throw new Error ("(BUG) choice.existingId cannot be null if choice.kind is 'existing'");
          this.programId = choice.existingId;
          this.programTitle = choice.title;
          this.cards_service.updateProgram (choice.existingId, this._program);
        }
        else if (choice && choice.kind == "new") {
          this.programTitle = choice.title;
          this.programId = this.cards_service.addProgram ({instructions: this._program || [], title: choice.title})
        }
        this.messages.showMessage ("Programma salvato sulla scheda " + this.programTitle, "success");
      });
    }
  }

  run_stop (trigger: MatMenuTrigger) {
    trigger.closeMenu ();
    if (!this.running) {
      this.sim_service.startPressed ();
    }
    else {
      console.debug ("[EditorComponent] abort program");
      this.sim_service.abortProgram ();
    }
  }
  
  run_from (fakelabel: number) {
    const fakelabel_to_label: Label[] = ['Z', 'V', 'W', 'Y'];
    const label = fakelabel_to_label[fakelabel];
    if (!label) throw new Error ("(BUG) wrong label in run_from. Fakelabel="+fakelabel);
    this.sim_service.labelPressed (label);
  }
  
  step (trigger: MatMenuTrigger) {
    trigger.closeMenu ();
    this.sim_service.runStep ();
  }
  
  step_from (fakelabel: number) {
    const fakelabel_to_label: Label[] = ['Z', 'V', 'W', 'Y'];
    const label = fakelabel_to_label[fakelabel];
    if (!label) throw new Error ("(BUG) wrong label in step_from. Fakelabel="+fakelabel);
    this.sim_service.runStepFromLabel (label);
  }

  private _close_all_the_menus () {
    // console.debug ("[EditorComponent] close all the menus triggers:", this.triggers);
    this.triggers.forEach ( t => {
      t.closeMenu();
    });
    this._menu_statuses['save'] = 0;
    this._menu_statuses['run'] = 0;
    this._menu_statuses['step'] = 0;
  }
  
  open_save_menu (trigger: MatMenuTrigger) {
    // save menu opens only if the user is editing a magnetic card and there is some code to save
    if (this.programId != null && this._program?.length) {
      this.open_menu ('save', trigger);
    }
  }
  
  open_fakelabels_menu (which: MenuKind, trigger: MatMenuTrigger) {
    // exec menus (run and step) opens only if the program has at least one fakelabel and the simulator is not running
    if (this._program_has_at_least_one_fakelabel && !this.sim_service.running) {
      this.open_menu (which, trigger);
    }
  }
  
  open_menu (which: MenuKind, trigger: MatMenuTrigger) {
    this._close_all_the_menus ();
    this._menu_statuses[which] = 1;
    // console.debug ("[EditorComponent] open menu", which," status", this._menu_statuses[which]);
    trigger.openMenu ();
  }

  close_menu (which: MenuKind, trigger: MatMenuTrigger) {
    this._menu_statuses[which] = 0;
    // console.debug ("[EditorComponent] close menu ", which, " status:", this._menu_statuses[which]);
    setTimeout ( () => {
      // console.debug ("[EditorComponent] close menu ", which, ". About to close menu. Menu status:",this._menu_statuses[which]);
      if (this._menu_statuses[which] == 0) {
        trigger.closeMenu ();
      }
    }, 500);
  }

  focus_menu (which: MenuKind) {
    this._menu_statuses[which] = 2;
    // console.debug ("[EditorComponent] focus menu", which, "status", this._menu_statuses[which] );
  }
  
  unfocus_menu (which: MenuKind, trigger: MatMenuTrigger) {
    this._menu_statuses[which] = 0;
    // console.debug ("[EditorComponent] unfocus menu", which, "status", this._menu_statuses[which]);
    setTimeout ( () => {
      // console.debug ("[EditorComponent] unfocus menu ", which, ". About to close menu. Menu status:",this._menu_statuses[which]);
      if (this._menu_statuses[which] == 0) {
        trigger.closeMenu();
      }
    }, 500);
  }

  private _detect_program_fakelabels () {
    const label_map = {'V': 1, 'W': 2, 'Y': 3, 'Z': 4};
    this.program_has_fakelabel = {1: false, 2: false, 3: false, 4: false};
    this._program_has_at_least_one_fakelabel = false;
    this._program?.forEach ( instr => {
      ['V', 'W', 'Y', 'Z'].forEach (label => {
        if (instr.register == 'A' && instr.operator == label) {
          this.program_has_fakelabel[label_map[label]] = true;
          this._program_has_at_least_one_fakelabel = true;
        }
      })
    });
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
    if (this.sim_service.running) {
      this.sim_service.abortProgram ();
    }
  }

}
