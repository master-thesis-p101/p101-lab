import { Component, OnInit } from '@angular/core';
import { MagneticCardsHolderService } from '@app/magnetic-cards-holder/magnetic-cards-holder.service';
import { Program } from '@app/magnetic-cards-holder';
import { Observable } from 'rxjs';
import { parse_program, program_to_string } from '@app/simulator';

// TODO: call cards_service.updateProgram when the title of a card changes

@Component({
  selector: 'app-magnetic-cards-holder',
  templateUrl: './magnetic-cards-holder.component.html',
  styleUrls: ['./magnetic-cards-holder.component.scss']
})
export class MagneticCardsHolderComponent implements OnInit {

  is_panel_open=true;
  selected_card: number | null = null;
  
  constructor ( public cards_service: MagneticCardsHolderService ) { }

  programs$: Observable<Program[]>;

  ngOnInit(): void {
    this.programs$ = this.cards_service.programs$;
  }

  drag_start ($event: DragEvent|null, id: number) {
    // console.debug ("[Holder-component] dragging started $event:", $event, "program id:", id);
    if ($event !== null && $event.dataTransfer) {
      $event.dataTransfer.setData('text/plain', ''+id);
      $event.dataTransfer.dropEffect = "copy";
    }
    this.cards_service.setSelectedCard (id);
    this.selected_card = id;
  }

  drag_end () {
    this.cards_service.setSelectedCard (null);
    this.selected_card = null;
  }

  load_program (id: number) {
    this.cards_service.loadProgram (id);
  }

  delete_program (id: number) {
    // TODO: confirmation dialog
    this.cards_service.deleteCard (id);
  }

  save_program (card: Program, a_elem: HTMLAnchorElement) {
    const program_str = program_to_string (card.instructions);
    const program_blob = new Blob([program_str], {type: 'text/plain'});
    a_elem.download = card.title + '.txt';
    const url = a_elem.href = window.URL.createObjectURL(program_blob);
    a_elem.click();
    setTimeout(() => {
       window.URL.revokeObjectURL(url);
    });
  }

  edit_program (id: number) {
    this.cards_service.editCard (id);
  }

  new_program () {
    this.cards_service.editCard (null);
  }

  upload_program (file_elm: HTMLInputElement) {
    file_elm.click ();
  }

  file_uploaded(file_elm: HTMLInputElement) {
    // console.debug ("[MagneticCardsHolderComponent] file uploaded. file_elm is", file_elm);
    if (file_elm.files && file_elm.files[0]) {
      const f = file_elm.files[0];
      f.text().then ( content => {
        try {
          const instructions = parse_program (content);
          const title = f.name.replace (/\.txt$/, "");
          this.cards_service.addProgram ({instructions, title});
        }
        catch (e) {
          // TODO: show error to the user
          console.log ("[[MagneticCardsHolderComponent] error while uploading the file", e);
        }
      });
    }
  }

}
