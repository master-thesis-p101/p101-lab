import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { SimulatorWithMessagesService } from '@app/simulatorWithMessages/simulator-with-messages.service';
import { Observable, Subscription } from 'rxjs';
import { scan, tap, auditTime } from 'rxjs/operators';

const MAX_PRINTER_LINES = 500;

@Component({
  selector: 'app-registers',
  templateUrl: './registers.component.html',
  styleUrls: ['./registers.component.scss']
})
export class RegistersComponent implements OnInit, OnDestroy {

  is_panel_open=true;
  private _subscription = new Subscription ();
  printerOutput$: Observable <string[]>;
  @ViewChild("printer") printer_output_container: ElementRef<HTMLElement>;
  decimals: number;

  constructor( public service: SimulatorWithMessagesService ) {
    this.decimals = service.decimals;
   }

  ngOnInit(): void {
    
    this._reset_printer_output ();

    const s1 = this.service.registersChanged$.subscribe ( changes => {
      // console.debug ("[RegistersComponent] registers changed. Changes:", changes);
      Object.keys(changes).forEach ( reg => {
        if (reg != "reg_M") {
          const elm = document.getElementById (reg);
          elm?.classList.add ("flashing");
          setTimeout ( () => {
            elm?.classList.remove ("flashing");
          }, 1000);
        } 
      });
    });

    const s2 = this.service.reset$.subscribe ( r => {
      this._reset_printer_output ();
    });
    
    this._subscription.add (s1);
    this._subscription.add (s2);
  }

  decimals_changed () {
    if (!this.decimals) {
      this.decimals = 0;
    }
    if (this.decimals > 15) {
      this.decimals = 15;
    } 
    if (this.decimals < 0) {
      this.decimals = 0;
    }
    this.service.numberOfDecimalsChanged (this.decimals);
  }

  general_reset_pressed () {
    this.service.resetPressed ();
  }

  get operator () {
    const o =this.service.lastArithmeticOperator;
    if (!o) {
      return '';
    }
    return o == 'sqrt' ? '√' : o;
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe ();
  }

  private _reset_printer_output () {
    this.printerOutput$ = this.service.printerOutput$.pipe (
      scan ((acc: string[], curr: string) => {
        acc.push (curr);
        if (acc.length > MAX_PRINTER_LINES) {
          acc = acc.slice (-MAX_PRINTER_LINES);
        }
        return acc;
      }, []),
      auditTime (200),
      tap ( _  => {
        setTimeout(() => {
          const el = this.printer_output_container.nativeElement;
          el.scrollBy (0, el.scrollHeight);
        });
      }),
    );
  }

}
