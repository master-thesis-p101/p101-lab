import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MagneticCardsHolderService } from '@app/magnetic-cards-holder/magnetic-cards-holder.service';
import { Subscription } from 'rxjs';

export interface ChosenCard {
  kind: 'new' | 'existing' | 'cancel',
  title: string,
  existingId: number | null,
}

@Component({
  selector: 'app-save-as-dialog',
  templateUrl: './save-as-dialog.component.html',
  styleUrls: ['./save-as-dialog.component.scss']
})
export class SaveAsDialogComponent implements OnInit {

  title: string;
  existing_titles: string[];
  private _subscription = new Subscription ();

  constructor(public service: MagneticCardsHolderService, public dialogRef: MatDialogRef<SaveAsDialogComponent>) { }

  ngOnInit(): void {
    const s1 = this.service.programs$.subscribe ( programs => {
      this.existing_titles = programs.map ( p => p.title );
    });
    this._subscription.add (s1);
  }

  save_new () {
    const out: ChosenCard = {kind: 'new', title: this.title, existingId: null};
    console.debug ("[SaveAsDialogComponent] save new", out);
    this.dialogRef.close (out);
  }
  
  save_existing (id: number, title: string) {
    const out: ChosenCard = {kind: 'existing', title, existingId: id};
    console.debug ("[SaveAsDialogComponent] overwrite existing", out);
    this.dialogRef.close (out);
  }
  
  cancel () {
    console.debug ("[SaveAsDialogComponent] cancel");
    this.dialogRef.close();
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe ();
  }

}
