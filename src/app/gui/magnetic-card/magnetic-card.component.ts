import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-magnetic-card',
  templateUrl: './magnetic-card.component.html',
  styleUrls: ['./magnetic-card.component.scss']
})
export class MagneticCardComponent implements OnInit {

  @Input() view_mode: 'compact' | 'extended' = 'compact';
  @Input() title: string;
  @Output() titleChange = new EventEmitter <string> ();

  constructor() { }

  ngOnInit(): void {
  }

  title_changed() {
    this.titleChange.emit (this.title);
  }

}
