import { Component, OnInit } from '@angular/core';
import { SimulatorWithMessagesService } from '@app/simulatorWithMessages/simulator-with-messages.service';

@Component({
  selector: 'app-numeric-keyboard',
  templateUrl: './numeric-keyboard.component.html',
  styleUrls: ['./numeric-keyboard.component.scss']
})
export class NumericKeyboardComponent implements OnInit {

  constructor(public sim_service: SimulatorWithMessagesService) { }

  ngOnInit(): void {
  }

}
