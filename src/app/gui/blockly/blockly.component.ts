import { Component, ElementRef, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { Instruction, parse_program } from '@app/simulator';
import * as Blockly from 'blockly';

import {BlocklyService, instructions_to_xml_str} from '@app/blockly';

import { Observable, Subscription } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, tap } from 'rxjs/operators';
import { SimulatorWithMessagesService } from '@app/simulatorWithMessages/simulator-with-messages.service';
import { blockly_options } from '@app/blockly/blockly-configurations';

const KEYBOARD_GAP_X = 16;
const KEYBOARD_GAP_Y = -40;

@Component({
  selector: 'app-blockly',
  templateUrl: './blockly.component.html',
  styleUrls: ['./blockly.component.scss']
})
export class BlocklyComponent implements OnInit, OnDestroy {

  private _subscription = new Subscription ();
  private _program_change = new EventEmitter <string | null> ();
  private _source_map: string[] = [];

  @ViewChild("blocklyArea") keyboard_container: ElementRef<HTMLElement>;
  show_keyboard = false;
  keyboard_x = 0;
  keyboard_y = 0;

  @Output() programChange = this._program_change.pipe (
    distinctUntilChanged ((a,b) => a==b),
    debounceTime (200),
    tap ( code => {
      this.show_keyboard = false;
      (this.workspace.highlightBlock as any) (null);
      if (code == null){
        this._source_map = [];
        return null;
      } 
      this._source_map = code.split ("\n").filter (l=>l).map ( l => l.split(" ")[1] );
      console.debug ("[BlocklyComponent] source map:", this._source_map);
    }),
    map ( program_str => program_str == null ?  null :
                         parse_program ( program_str.split ("\n").map ( l => l.split(" ")[0] ).join ("\n") )),
  );
  @Input() setProgram$: Observable<Instruction[]>;

  workspace: Blockly.WorkspaceSvg;

  constructor( public blockly_service: BlocklyService, public sim_service: SimulatorWithMessagesService ) { }

  ngOnInit(): void {
    //TODO: maybe resize the workspace when the window resizes. It does not seem necessary right now
    this.workspace = Blockly.inject('blockly-area', blockly_options);
    this.workspace.addChangeListener(this._handle_code_changed.bind(this));

    const s1 = this.setProgram$.subscribe ( p=> {
      const program_xml = instructions_to_xml_str (p);
      const ws_dom = Blockly.Xml.textToDom (program_xml);
      Blockly.Xml.clearWorkspaceAndLoadFromXml (ws_dom, this.workspace);
    });

    const s2 = this.sim_service.executionStopped$.subscribe (stop_at => {
      if (stop_at != null && (stop_at < 0 || stop_at >= this._source_map.length) ) throw new Error ("(BUG) machine stopped on instruction outside the program " + stop_at);
      const id_block = stop_at == null ?  null : this._source_map[stop_at];
      // TODO: remove "as any" when they will fix this.
      //       parameter id can be null: https://developers.google.com/blockly/reference/js/Blockly.WorkspaceSvg#highlightBlock
      //       the type annotation for highlightBlock is incorrect.
      (this.workspace.highlightBlock as any) (id_block);
      console.debug ("[BlocklyComponent] execution stopped at instruction", stop_at, "<----> block id", id_block);
      this.show_keyboard = false;
      if (id_block != null) {
        const b = this.workspace.getBlockById (id_block);
        if (b.type == "S") {
          console.debug ("[BlocklyComponent] stop on an input instruction, displaying keyboard");
          this._compute_keyboard_position (b.getRelativeToSurfaceXY ().x + (b as Blockly.BlockSvg).width, b.getRelativeToSurfaceXY ().y + (b as Blockly.BlockSvg).height/2);
          // this.keyboard_x = this._keyboard_base_position.x + b.getRelativeToSurfaceXY ().x + (b as Blockly.BlockSvg).width;
          // this.keyboard_y = this._keyboard_base_position.y + b.getRelativeToSurfaceXY ().y + (b as Blockly.BlockSvg).height/2;
          this.show_keyboard = true;
        }
      }
    });

    const s3 = this.sim_service.reset$.subscribe ( r => {
      if (r) {
        this.show_keyboard = false;
        (this.workspace.highlightBlock as any) (null);
      }
    });

    this._subscription.add (s1);
    this._subscription.add (s2);
    this._subscription.add (s3);
  }

  private _handle_code_changed () {
    const code_generator = ((Blockly as any).JavaScript as Blockly.Generator);
    const code: string = code_generator.workspaceToCode(this.workspace);
    // console.debug ("[BlocklyComponent] code changed. New code:");
    // console.debug (code);
    if ( code.search ("\n\n") == -1 ) {
      // console.debug ("[BlocklyComponent] single pile: emit code directly");
      this._program_change.emit ( code );
    }
    else {
      // console.debug("[BlocklyComponent] multiple piles, emit code if one selected, otherwise emit null");
      if (Blockly.selected) {
        const selected = (Blockly.selected as unknown as  Blockly.Block);
        const pile_code: string = code_generator.blockToCode (selected.getRootBlock()) as string;
        // console.debug("[BlocklyComponent] selected pile code:", pile_code);
        this._program_change.emit (pile_code);
      }
      else {
        this._program_change.emit (null);
      }
    }
  }

  private _compute_keyboard_position (dx: number, dy: number) {
    // console.debug ("[Blockly-component] base rectangle", this.keyboard_container.nativeElement.getBoundingClientRect());
    console.debug ("[Blockly-component] toolbox width:", this.workspace.getToolbox()?.getWidth());
    // console.debug ("[Blockly-component] workspace drag x,y", this.workspace.scrollX, this.workspace.scrollY);
    // FIXME: getToolbox() is null when there are no categories even if the toolbox exists
    const width = this.workspace.getToolbox() != null ? this.workspace.getToolbox().getWidth() : 256;
    this.keyboard_x = dx + window.scrollX + this.keyboard_container.nativeElement.getBoundingClientRect().left + this.workspace.scrollX + width + KEYBOARD_GAP_X; 
    this.keyboard_y = dy + window.scrollY + this.keyboard_container.nativeElement.getBoundingClientRect().top + this.workspace.scrollY + KEYBOARD_GAP_Y;
  }

  ngOnDestroy(): void {
    this._subscription.unsubscribe();
  }

}
