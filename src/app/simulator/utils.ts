
import {Instruction, Operator, Register} from './models';

export function parse_program ( program_str: string ){
    const lines = program_str.split ("\n");
    const out: Instruction[] = [];
    lines.filter( l => l ).forEach ( (l: string, i: number) => {
        const line_parts = l.split(/\s*,\s*/);
        let str_register: string;
        let str_operator: string;
        let operator: Operator;
        let register: Register;
        if (line_parts.length == 2) {
            str_register = line_parts[0];
            str_operator = line_parts[1];
        } else if (line_parts.length == 3) {
            // this function can also parse programs written by the original simulator ignoring the "constant" column
            str_register = line_parts[1];
            str_operator = line_parts[2];
        } else {
            throw new Error ("parse_program: Wrong coumns count at line "+(i+1)+": "+l);
        }

        if ( -1 == ['A', 'B', 'C', 'D', 'E', 'F', 'R', 'M', 'a', 'b', 'c', 'd', 'e', 'f', '', 'r', '/'].indexOf(str_register) ) {
            throw new Error("parse_program: Wrong register " + str_register + " at line "+(i+1)+": "+l);
        }
        register = str_register as Register;

        // convert original simulator operators to new ones
        const old_operators_map = {'sum': '+', 'sub': '-', 'mult': 'x', 'div': ':'};
        if (old_operators_map[str_operator]) str_operator = old_operators_map[str_operator];
            
        if ( -1 == ['+', '-' , 'x', ':', 'print', 'clear', '*', 'S', 'sqrt', 'V', 'W', 'Y', 'Z', 'fromM', 'toA', 'exchange'].indexOf(str_operator) ) {
            throw new Error("parse_program: Wrong operator " + str_operator + "  at line "+(i+1)+": "+l);
        }
        operator = str_operator as Operator;

        out.push ({register, operator});
        
    });

    return out;
}

export function program_to_string ( program: Instruction[] ) {
    if (program.length)  return program.map (i => i.register + ',' + i.operator ).reduce ( (a,b) => a+"\n" + b );
    else return "";
}
