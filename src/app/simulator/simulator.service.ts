import { Injectable } from '@angular/core';

import { ArithmeticOperator, Instruction, Label, P101Simulator, Register } from './models';
import { UnicasSimulator } from './unicas_code/UnicasSimulator';
import { createMessageForWorker, deserializaMachineStatus, MessageFromSimulatorWorkerToService } from './models-messages';
import { Subject } from 'rxjs';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class SimulatorService {

  private simulator: P101Simulator;
  private worker: Worker;
  private _printer_output$ = new Subject<string> ();
  private _error$ = new Subject<string | null> ();
  private _reset$ = new Subject<boolean> ();
  private _execution_stopped$ = new Subject<number | null> ();
  private _execution_started$ = new Subject<boolean> ();

  constructor() {
    // FIXME: this shouldn't be necessary
    //        remove when figured out to run web workers into the unit test
    if (environment.test) {
      // web workers do not work into unit tests, failing back to the synchronous version
      this.simulator = new UnicasSimulator(false);
    }
    else {
      this.simulator = new UnicasSimulator(true);
      if (typeof Worker !== 'undefined') {
        this.worker = new Worker(new URL('./simulator.worker', import.meta.url));
        this.worker.onmessage = this._handle_worker_message.bind(this);
      } else {
        console.warn ("Web Workers are not supported in this browser: switching to the synchronous mode");
        this.simulator = new UnicasSimulator(false);
      }
    }
  }

  private _handle_worker_message ( m: {data: MessageFromSimulatorWorkerToService} ) {
    console.debug ("[SimulatorService] received machine status from worker, updating the simulator");
    const new_status = deserializaMachineStatus (m.data.status);
    this.simulator.setInternalStatus (new_status, m.data.error);
    m.data.output.forEach (line => {
      this._printer_output$.next (line);
    });
    this._update_error_and_printer_output ();
    const stop_at = this.simulator.machineStatus.currentInstruction == null ? null : this.simulator.machineStatus.currentInstruction - 1;
    if (!this.running) {
      this._execution_stopped$.next(stop_at);
    }
  }

  private _start_simulator_in_worker() {
    const m = createMessageForWorker (this.simulator);
    this._execution_started$.next (true);
    this.worker.postMessage(m);
  }

  private _update_error_and_printer_output () {
    this.simulator.printerOutput.forEach (line => {
      this._printer_output$.next (line);
    });
    this._error$.next (this.error);
  }

  digitPressed (d: number) {
    this.simulator.digitPressed (d);
    this._update_error_and_printer_output ();
  }

  dotPressed () {
    this.simulator.dotPressed();
    this._update_error_and_printer_output ();              
  }

  algebraicSignPressed () {
    this.simulator.algebraicSignPressed();
    this._update_error_and_printer_output ();
  }

  startPressed () {
    this.simulator.startPressed();
    this._update_error_and_printer_output ();
    if (this.simulator.needsToBeStarted) {
      this._start_simulator_in_worker();
    }
    else {
      const stop_at = this.simulator.machineStatus.currentInstruction == null ? null : this.simulator.machineStatus.currentInstruction - 1;
      this._execution_stopped$.next(stop_at);
    }
  }

  clearPressed () {
    this.simulator.clearPressed();
    this._update_error_and_printer_output ();
  }

  resetPressed () {
    this.simulator.resetPressed();
    this._reset$.next (true);
    this._update_error_and_printer_output ();
  }

  registerPressed (r: Register) {
    this.simulator.registerPressed(r);
    this._update_error_and_printer_output ();
  }

  clearRegisterPressed () {
    this.simulator.clearRegisterPressed();
    this._update_error_and_printer_output (); 
  }

  arithmeticOperatorPressed (o: ArithmeticOperator) {
    this.simulator.arithmeticOperatorPressed(o);
    this._update_error_and_printer_output ();      
  }

  printPressed () {
    this.simulator.printPressed();
    this._update_error_and_printer_output ();
  }

  fromMPressed () {
    this.simulator.fromMPressed();
    this._update_error_and_printer_output ();
  }

  toAPressed () {
    this.simulator.toAPressed();
    this._update_error_and_printer_output ();
  }

  exchangePressed () {
    this.simulator.exchangePressed();
    this._update_error_and_printer_output ();
  }

  recordPrToggled () {
    this.simulator.recordPrToggled();
  }

  printPrToggled () {
    this.simulator.printPrToggled();
  }

  labelPressed (l: Label) {
    this.simulator.labelPressed(l);
    this._update_error_and_printer_output ();
    if (this.simulator.needsToBeStarted) {
      this._start_simulator_in_worker ();
    }
    else {
      const stop_at = this.simulator.machineStatus.currentInstruction == null ? null : this.simulator.machineStatus.currentInstruction - 1;
      this._execution_stopped$.next(stop_at);
    }
  }

  slashPressed () {
    this.simulator.slashPressed();
    this._update_error_and_printer_output ();
  }

  // FIXME: wrong printer output when mixing startPressed() and runStep()
  //        runStep() on S instruction should print as if S was pressed
  runStep () {
    this.simulator.runStep ();
    const stop_at = this.simulator.machineStatus.currentInstruction == null ? null : this.simulator.machineStatus.currentInstruction - 1;
    this._execution_stopped$.next(stop_at);
    this._update_error_and_printer_output ();
  }

  runStepFromLabel(l: Label) {
    this.simulator.runStepFromLabel (l);
    const stop_at = this.simulator.machineStatus.currentInstruction == null ? null : this.simulator.machineStatus.currentInstruction - 1;
    this._execution_stopped$.next(stop_at);
    this._update_error_and_printer_output ();
  }

  abortProgram () {
    let out = false;
    if (this.running) {
      this.worker.terminate ();
      this._execution_stopped$.next (null);
      this.simulator.machineStatus.running = false;
      this.worker = new Worker(new URL('./simulator.worker', import.meta.url));
      this.worker.onmessage = this._handle_worker_message.bind(this);
      out = true;
    }
    return out;
  }

  numberOfDecimalsChanged (n: number) {
    this.simulator.numberOfDecimalChanged(n);
  }

  set program_code ( c: Instruction[] ) {
    this.simulator.loadMagneticCard (c);
  }

  get program_code () {
    return this.simulator.writeMagneticCard ();
  }

  get registers () {
    return this.simulator.machineStatus.registers;
  }

  get error () {
    return this.simulator.error;
  }
  
  get running () {
    return this.simulator.running;
  }

  get selectedRegister () {
    return this.simulator.selectedRegister;
  }

  get lastArithmeticOperator () {
    return this.simulator.lastArithmeticOperator;
  }

  get printerOutput$ () {
    return this._printer_output$.asObservable ();
  }

  get error$ () {
    return this._error$.asObservable ();
  }

  get pendingStringRegisterM () {
    return this.simulator.pendingStringRegisterM;
  }

  get registersChanged$ () {
    return this.simulator.registersChanged$;
  }

  get reset$ () {
    return this._reset$.asObservable ();
  }

  get decimals () {
    return this.simulator.machineStatus.numberOfDecimals;
  }

  get executionStopped$ () {
    return this._execution_stopped$.asObservable ();
  }
  
  get executionStarted$ () {
    return this._execution_started$.asObservable ();
  }

}
