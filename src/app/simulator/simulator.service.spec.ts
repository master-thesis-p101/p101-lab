import { TestBed } from '@angular/core/testing';
import { Big } from 'big.js';

import { SimulatorService } from './simulator.service';
import { RegisterValues } from './models';
import { parse_program } from './utils';

// TODO: test printer output
// TODO: test printProgram after writing two programs (10 instructions, then 3 instructions)
// TODO: test labelPressed (V,W,Y,Z), runStepFromLabel()

type expectedValues = { 
  reg_M?: number | Big | null;
  reg_A?: number | Big | null;
  reg_R?: number | Big | null;
  reg_r?: number | Big | null;
  reg_B?: number | Big | null;
  reg_b?: number | Big | null;
  reg_C?: number | Big | null;
  reg_c?: number | Big | null;
  reg_D?: number | Big | null;
  reg_d?: number | Big | null;
  reg_E?: number | Big | null;
  reg_e?: number | Big | null;
  reg_F?: number | Big | null;
  reg_f?: number | Big | null;
}

// I know, the name sucks, but I wanted to keep jasmine spirit alive 
function theseRegisterValues (expected: expectedValues) {
  return {
    
    /**
     * compares the parameter with the expected register values and returns true if and only if they match.
     * if any register is not specified in the expected values it is not involved in the comparison.
     * */
    asymmetricMatch: (compareTo: RegisterValues) =>
      Object.keys (expected).every (reg_name => {
        const actual_value = compareTo[reg_name];
        if (expected[reg_name] == null) {
          return actual_value == null;
        }
        else {
          return actual_value != null && actual_value.eq (expected[reg_name]);
        }
      }),

    jasmineToString: () => {
      const reg_value_string = Object.keys (expected)
          .map (k => k + ': ' + (expected[k] == null ? 'null' : expected[k].toString()))
          .reduce ((a,b) => a+"\n"+b);
      return '<these register values: {\n' + reg_value_string + '\n}>'
    }
  };
}


describe('SimulatorService tests', () => {
  let service: SimulatorService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SimulatorService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should handle inputs correctly #1', () => {
    [1,2,3,4,5].forEach (d => service.digitPressed (d));
    expect (service.pendingStringRegisterM).toEqual ("");
    expect (service.registers).toEqual (theseRegisterValues({reg_M: 12345}));
    service.printPressed ();
    service.digitPressed (6); service.digitPressed (7);
    expect (service.pendingStringRegisterM).toEqual ("");
    expect (service.registers).toEqual (theseRegisterValues({reg_M: 67}));
  });
  
  it('should handle inputs correctly #2', () => {
    [7,4,9].forEach (d => service.digitPressed (d));
    service.dotPressed ();
    [4,5,1,0].forEach (d => service.digitPressed (d));
    expect (service.pendingStringRegisterM).toEqual ("0");
    expect (service.registers).toEqual (theseRegisterValues({reg_M: 749.451}));
  });
  
  it('should handle inputs correctly #3', () => {
    service.digitPressed (3);
    service.dotPressed ();
    [0,0,0,0].forEach (d => service.digitPressed (d));
    expect (service.pendingStringRegisterM).toEqual (".0000");
    expect (service.registers).toEqual (theseRegisterValues({reg_M: 3}));
    [4,5,6].forEach (d => service.digitPressed (d));
    expect (service.pendingStringRegisterM).toEqual ("");
    expect (service.registers).toEqual (theseRegisterValues({reg_M: 3.0000456}));
  });

  it('should move values between registers correctly', () => {
    service.digitPressed(8); service.digitPressed(8); 
    service.toAPressed(); service.registerPressed('B'); service.fromMPressed();
    service.digitPressed(5); service.digitPressed(5);
    service.registerPressed('E'); service.fromMPressed();
    service.digitPressed(9); service.digitPressed(9);
    service.registerPressed('F'); service.fromMPressed();
    service.registerPressed('F'); service.toAPressed();
    service.registerPressed('E'); service.exchangePressed();
    expect (service.registers).toEqual (theseRegisterValues({
      reg_M: 99,
      reg_A: 55,
      reg_B: 88,
      reg_E: 99,
      reg_F: 99,
    }));
  });

  it('should handle R exchange correctly', () => {
    service.digitPressed(4); service.digitPressed(4); service.digitPressed(5); service.toAPressed();
    service.digitPressed(2); service.arithmeticOperatorPressed('+');
    service.digitPressed(3); service.digitPressed(2); service.digitPressed(1); service.toAPressed();
    service.registerPressed ('R'); service.exchangePressed ();
    expect (service.registers).toEqual (theseRegisterValues({
      reg_M: 321,
      reg_A: 447,
      reg_R: 447,
    }));
  });

  it('should compute sums correctly', () => {
    service.digitPressed(8); service.digitPressed(8); service.toAPressed();
    service.digitPressed(3); service.digitPressed(3); service.arithmeticOperatorPressed('+');
    expect (service.registers).toEqual (theseRegisterValues({
      reg_M: 33,
      reg_A: 121,
      reg_R: 121,
    }));
  });
  
  it('should compute subtractions correctly', () => {
    service.digitPressed(8); service.digitPressed(8); service.toAPressed();
    service.digitPressed(3); service.digitPressed(3); service.arithmeticOperatorPressed('-');
    expect (service.registers).toEqual (theseRegisterValues({
      reg_M: 33,
      reg_A: 55,
      reg_R: 55,
    }));
  });
  
  it('should compute multiplications correctly', () => {
    service.digitPressed(8); service.digitPressed(8); service.toAPressed();
    service.digitPressed(3); service.digitPressed(3); service.arithmeticOperatorPressed('x');
    expect (service.registers).toEqual (theseRegisterValues({
      reg_M: 33,
      reg_A: 2904,
      reg_R: 2904,
    }));
  });
  
  it('should compute divisions correctly', () => {
    service.digitPressed(8); service.digitPressed(8); service.toAPressed();
    service.digitPressed(3); service.digitPressed(3); service.arithmeticOperatorPressed(':');
    expect (service.registers).toEqual (theseRegisterValues({
      reg_M: 33,
      reg_A: 2.66,
      reg_R: new Big ("0.22"),
    }));
  });

  it('should compute square roots correctly', () => {
    service.digitPressed(1); service.digitPressed(2); service.digitPressed(1); service.toAPressed();
    service.arithmeticOperatorPressed('sqrt');
    expect (service.registers).toEqual (theseRegisterValues({
      reg_M: 22,
      reg_A: 11,
    }));
  });
  
  it('should clear registers correctly', () => {
    service.digitPressed(1); service.digitPressed(2); service.digitPressed(3); service.toAPressed();
    service.registerPressed('B'); service.fromMPressed();
    service.registerPressed('C'); service.fromMPressed();
    expect (service.registers).toEqual (theseRegisterValues({
      reg_M: 123,
      reg_A: 123,
      reg_B: 123,
      reg_C: 123,
    }));
    service.registerPressed ('B'); service.clearRegisterPressed ();
    service.registerPressed ('A'); service.clearRegisterPressed ();
    expect (service.registers).toEqual (theseRegisterValues({
      reg_M: 123,
      reg_A: 0,
      reg_B: null,
      reg_C: 123,
    }));
  });

  it("should truncate decimals instead of rounding", () => {
    service.numberOfDecimalsChanged (0);
    service.digitPressed(1); service.digitPressed(5); service.digitPressed(4); service.digitPressed(1); service.digitPressed(3);
    service.toAPressed()
    service.digitPressed(1); service.digitPressed(5); service.digitPressed(4); service.digitPressed(3);
    service.arithmeticOperatorPressed (':');
    expect (service.registers).toEqual (theseRegisterValues({ reg_M: 1543, reg_A: 9, reg_R: 1526}));
  });
  
  // #differentBehavior the original simulator resets R
  it("should not clear register R", () => {
    service.digitPressed(1); service.digitPressed(2); service.digitPressed(3); service.toAPressed();
    service.digitPressed(0); service.arithmeticOperatorPressed('+');
    expect (service.registers).toEqual (theseRegisterValues({ reg_M: 0, reg_A: 123, reg_R: 123}));
    service.registerPressed('R'); service.clearRegisterPressed ();
    expect (service.registers).toEqual (theseRegisterValues({ reg_M: 0, reg_A: 123, reg_R: 123}));
  });
  
  // #differentBehavior the original simulator set A to null thus it is impossible to perform further computations
  it("should perform further calculations after resetting A", () => {
    service.digitPressed(1); service.digitPressed(2); service.digitPressed(3); service.toAPressed();
    service.digitPressed(0); service.arithmeticOperatorPressed('+');
    expect (service.registers).toEqual (theseRegisterValues({ reg_M: 0, reg_A: 123, reg_R: 123}));
    service.registerPressed('A'); service.clearRegisterPressed ();
    expect (service.registers).toEqual (theseRegisterValues({ reg_M: 0, reg_A: 0, reg_R: 123}));
    service.digitPressed(4); service.digitPressed(5); service.arithmeticOperatorPressed('+');
    expect (service.registers).toEqual (theseRegisterValues({ reg_M: 45, reg_A: 45, reg_R: 45}));
  });

  // #differentBehavior the original simulator rounds
  it("should truncate decimals instead of rounding", () => {
    service.numberOfDecimalsChanged (1);
    service.digitPressed(1); service.digitPressed(2);
    service.arithmeticOperatorPressed('sqrt');
    expect (service.registers).toEqual (theseRegisterValues({ reg_M: 6.8, reg_A: 3.4}));
  });

  // #differentBehavior the original simulator is affected by machine precision
  it("should compute the exact value in the register R", () => {
    service.digitPressed(9); service.toAPressed ();
    service.digitPressed(0); service.arithmeticOperatorPressed ('+');
    service.digitPressed(8); service.dotPressed (); service.digitPressed(4); service.digitPressed(3); service.digitPressed(9); service.digitPressed(7); service.registerPressed('B'); service.fromMPressed();
    service.digitPressed(6); service.dotPressed (); service.digitPressed(1); service.digitPressed(8); service.toAPressed();
    service.digitPressed(5); service.dotPressed (); service.digitPressed(4); service.digitPressed(3); service.digitPressed(2); service.digitPressed(1);
    expect (service.registers).toEqual (theseRegisterValues({ reg_M: 5.4321, reg_A: 6.18, reg_R: 9, reg_B: 8.4397}));
    service.registerPressed('B'); service.arithmeticOperatorPressed('+');
    expect (service.registers).toEqual (theseRegisterValues({ reg_M: 8.4397, reg_A: 14.61, reg_R: 14.6197, reg_B: 8.4397}));
  });

  // #differentBehavior the original simulator registers an empty program
  it("should be able to register another program after the first one", () => {
    service.recordPrToggled();
    service.startPressed (); service.startPressed (); service.startPressed (); service.startPressed (); service.startPressed (); 
    service.recordPrToggled();
    const first_prog = service.program_code;
    expect(first_prog.length).toBe(5);
    for (let j=0; j<first_prog.length; ++j) {
      expect(first_prog[j]).toEqual ({operator: 'S', register: ''});
    }

    service.recordPrToggled();
    service.startPressed (); 
    service.recordPrToggled();
    const second_prog = service.program_code;
    expect(second_prog.length).toBe(1);
    expect(first_prog[0]).toEqual ({operator: 'S', register: ''});
    
  });

  // #differentBehavior the original simulator is affected by machine precision
  it("should compute the divisions reminders correctly", () => {
    service.digitPressed(1); service.digitPressed(1); service.digitPressed(1); service.digitPressed(1); service.toAPressed ();
    service.digitPressed(6); service.digitPressed(8); service.arithmeticOperatorPressed (':');
    expect (service.registers).toEqual (theseRegisterValues({ reg_A: 16.33, reg_M: 68, reg_R: 0.56}));
  });


  it('should handle sequences of commands correctly #1', () => {
    service.digitPressed(7); service.digitPressed(9); service.toAPressed();
    service.digitPressed(5); service.digitPressed(5); service.registerPressed('B'); service.fromMPressed();
    service.digitPressed(2); service.digitPressed(2); service.registerPressed('C'); service.fromMPressed();
                                   service.arithmeticOperatorPressed ('+');
    service.registerPressed ('B'); service.arithmeticOperatorPressed ('-');
    service.registerPressed ('C'); service.exchangePressed();
                                   service.arithmeticOperatorPressed ('sqrt');
    service.registerPressed ('C'); service.arithmeticOperatorPressed ('+');
    service.registerPressed ('D'); service.fromMPressed();
    expect (service.registers).toEqual (theseRegisterValues({
      reg_M: 46,
      reg_A: 53.41,
      reg_R: 53.41,
      reg_B: 55,
      reg_C: 46,
      reg_D: 46,
    }));
  });

  it('should handle sequences of commands correctly #2', () => {
    service.digitPressed(6); service.digitPressed(6); service.toAPressed();
    service.digitPressed(2); service.registerPressed('D'); service.fromMPressed();
    service.digitPressed(5); service.digitPressed(5); service.digitPressed(5); service.dotPressed(); service.digitPressed(5); service.arithmeticOperatorPressed(':');
    service.registerPressed('D'); service.arithmeticOperatorPressed('+');
    service.registerPressed('B'); service.exchangePressed();
    service.numberOfDecimalsChanged(3);
    service.registerPressed('R'); service.toAPressed();
    service.arithmeticOperatorPressed(':');
    service.registerPressed('D'); service.clearRegisterPressed();
    expect (service.registers).toEqual (theseRegisterValues({
      reg_M: 2,
      reg_A: 1.055,
      reg_R: 0,
      reg_B: 2.11,
      reg_D: null,
    }));
  });

  it('should handle general reset correctly', () => {
    service.numberOfDecimalsChanged (9);
    service.digitPressed(7); service.digitPressed(9); service.toAPressed();
    service.digitPressed(5); service.digitPressed(5); service.registerPressed('B'); service.fromMPressed();
    service.digitPressed(2); service.digitPressed(2); service.registerPressed('C'); service.fromMPressed();
                                   service.arithmeticOperatorPressed ('+');
    service.registerPressed ('B'); service.arithmeticOperatorPressed ('-');
    service.registerPressed ('C'); service.exchangePressed();
                                   service.arithmeticOperatorPressed ('sqrt');
    service.registerPressed ('C'); service.arithmeticOperatorPressed ('+');
    service.registerPressed ('D'); service.fromMPressed();
    service.resetPressed ();
    expect (service.registers).toEqual (theseRegisterValues({
      reg_M: 0,
      reg_A: 0,
      reg_R: null,
      reg_B: null,
      reg_C: null,
      reg_D: null,
      reg_E: null,
      reg_F: null,
    }));
  });
  
  it('should change the number of decimal digits correctly', ()=>{
    service.digitPressed(1); service.digitPressed(0); service.toAPressed();
    service.digitPressed(1); service.digitPressed(5); service.arithmeticOperatorPressed(':');
    expect (service.registers).toEqual (theseRegisterValues({ reg_A: 0.66, reg_R: 0.1 }));
    
    service.numberOfDecimalsChanged (1);
    service.digitPressed(1); service.digitPressed(0); service.toAPressed();
    service.digitPressed(1); service.digitPressed(5); service.arithmeticOperatorPressed(':');
    expect (service.registers).toEqual (theseRegisterValues({ reg_A: 0.6, reg_R: 1 }));

    service.numberOfDecimalsChanged (0);
    service.digitPressed(1); service.digitPressed(0); service.toAPressed();
    service.digitPressed(1); service.digitPressed(5); service.arithmeticOperatorPressed(':');
    expect (service.registers).toEqual (theseRegisterValues({ reg_A: 0, reg_R: 10 }));

    service.numberOfDecimalsChanged (3);
    service.digitPressed(1); service.digitPressed(0); service.toAPressed();
    service.digitPressed(1); service.digitPressed(5); service.arithmeticOperatorPressed(':');
    expect (service.registers).toEqual (theseRegisterValues({ reg_A: 0.666, reg_R: 0.01 }));

    service.numberOfDecimalsChanged (5);
    service.digitPressed(1); service.digitPressed(0); service.toAPressed();
    service.digitPressed(1); service.digitPressed(5); service.arithmeticOperatorPressed(':');
    expect (service.registers).toEqual (theseRegisterValues({ reg_A: 0.66666, reg_R: 0.0001 }));

    service.numberOfDecimalsChanged (8);
    service.digitPressed(1); service.digitPressed(0); service.toAPressed();
    service.digitPressed(1); service.digitPressed(5); service.arithmeticOperatorPressed(':');
    expect (service.registers).toEqual (theseRegisterValues({ reg_A: 0.66666666, reg_R: 0.0000001 }));
    
    service.numberOfDecimalsChanged (15);
    service.digitPressed(1); service.digitPressed(0); service.toAPressed();
    service.digitPressed(1); service.digitPressed(5); service.arithmeticOperatorPressed(':');
    expect (service.registers).toEqual (theseRegisterValues({ reg_A: 0.666666666666666, reg_R: 0.00000000000001 }));
  });

  it('should register a program correctly', () => {
    service.recordPrToggled ();
    service.registerPressed('A'); service.slashPressed(); service.labelPressed('V');
    service.registerPressed('D'); service.toAPressed();
    service.registerPressed ('A'); service.arithmeticOperatorPressed(':');
    service.registerPressed ('D'); service.exchangePressed();
    service.registerPressed ('D'); service.arithmeticOperatorPressed('-');
    service.registerPressed ('D'); service.exchangePressed();
    service.registerPressed ('D'); service.toAPressed();
    service.registerPressed ('A'); service.printPressed();
    service.slashPressed(); service.labelPressed('V');
    service.slashPressed(); service.printPressed();
    service.recordPrToggled();

    const prog = service.program_code;
    expect (prog.length).toEqual (10);
    expect (prog[0]).toEqual ({register: 'a', operator: 'V'});
    expect (prog[1]).toEqual ({register: 'D', operator: 'toA'});
    expect (prog[2]).toEqual ({register: 'A', operator: ':'});
    expect (prog[3]).toEqual ({register: 'D', operator: 'exchange'});
    expect (prog[4]).toEqual ({register: 'D', operator: '-'});
    expect (prog[5]).toEqual ({register: 'D', operator: 'exchange'});
    expect (prog[6]).toEqual ({register: 'D', operator: 'toA'});
    expect (prog[7]).toEqual ({register: 'A', operator: 'print'});
    expect (prog[8]).toEqual ({register: '/', operator: 'V'});
    expect (prog[9]).toEqual ({register: '/', operator: 'print'});    
  });

  it ('should encode constants correctly', () => {
    service.recordPrToggled();
    
    service.digitPressed(2); service.digitPressed(5); service.digitPressed(0); service.dotPressed(); service.digitPressed(3); service.digitPressed(4);
    service.registerPressed('A'); service.slashPressed(); service.fromMPressed();
    
    service.digitPressed(1); service.dotPressed(); service.digitPressed(5); service.digitPressed(5);
    service.registerPressed('A'); service.slashPressed(); service.fromMPressed();
    
    service.digitPressed(1); service.digitPressed(4); service.digitPressed(4); service.dotPressed(); service.algebraicSignPressed(); service.digitPressed(3); service.digitPressed(2); service.digitPressed(5);
    service.registerPressed('A'); service.slashPressed(); service.fromMPressed();

    service.algebraicSignPressed(); service.digitPressed(0); service.dotPressed(); service.digitPressed(4); service.digitPressed(8); service.digitPressed(9); service.digitPressed(1);
    service.registerPressed('A'); service.slashPressed(); service.fromMPressed();
    
    service.digitPressed(1); service.digitPressed(0);
    service.registerPressed('A'); service.slashPressed(); service.fromMPressed();

    service.digitPressed(1); service.digitPressed(0); service.algebraicSignPressed();
    service.registerPressed('A'); service.slashPressed(); service.fromMPressed();
    
    service.recordPrToggled();

    const prog = service.program_code;
    let i=0;
    expect (prog.length).toEqual (29);
    
    expect (prog[i++]).toEqual ({register: 'a', operator: 'fromM'});
    expect (prog[i++]).toEqual ({register: 'R', operator: '+'});
    expect (prog[i++]).toEqual ({register: 'R', operator: 'exchange'});
    expect (prog[i++]).toEqual ({register: 'r', operator: 'S'});
    expect (prog[i++]).toEqual ({register: 'R', operator: '-'});
    expect (prog[i++]).toEqual ({register: 'D', operator: 'fromM'});

    expect (prog[i++]).toEqual ({register: 'a', operator: 'fromM'});
    expect (prog[i++]).toEqual ({register: 'R', operator: '-'});
    expect (prog[i++]).toEqual ({register: 'R', operator: '-'});
    expect (prog[i++]).toEqual ({register: 'd', operator: 'toA'});

    expect (prog[i++]).toEqual ({register: 'a', operator: 'fromM'});
    expect (prog[i++]).toEqual ({register: 'F', operator: '-'});
    expect (prog[i++]).toEqual ({register: 'F', operator: 'fromM'});
    expect (prog[i++]).toEqual ({register: 'F', operator: 'exchange'});
    expect (prog[i++]).toEqual ({register: 'f', operator: '+'});
    expect (prog[i++]).toEqual ({register: 'F', operator: '+'});
    expect (prog[i++]).toEqual ({register: 'E', operator: 'toA'});
    
    expect (prog[i++]).toEqual ({register: 'a', operator: 'fromM'});
    expect (prog[i++]).toEqual ({register: 'F', operator: 'toA'});
    expect (prog[i++]).toEqual ({register: 'F', operator: '*'});
    expect (prog[i++]).toEqual ({register: 'F', operator: 'print'});
    expect (prog[i++]).toEqual ({register: 'F', operator: '+'});
    expect (prog[i++]).toEqual ({register: 'e', operator: 'S'});

    expect (prog[i++]).toEqual ({register: 'a', operator: 'fromM'});
    expect (prog[i++]).toEqual ({register: 'r', operator: 'S'});
    expect (prog[i++]).toEqual ({register: 'D', operator: 'toA'});

    expect (prog[i++]).toEqual ({register: 'a', operator: 'fromM'});
    expect (prog[i++]).toEqual ({register: 'f', operator: 'S'});
    expect (prog[i++]).toEqual ({register: 'E', operator: 'toA'});

  });

  it ('should decode encoded constants correctly', () => {
    for (let j=0; j<10; ++j) {
      const r = (Math.random()*2000 - 1000).toFixed (Math.floor(Math.random()*10));
      service.resetPressed ();
      service.recordPrToggled ();
      for (let i=0; i<r.length; ++i) {
        if (r[i] == '.') {
          service.dotPressed ();
        }
        else if (r[i] == '-') {
          service.algebraicSignPressed ();
        }
        else {
          service.digitPressed (+r[i]);
        }
      }
      service.registerPressed('A'); service.slashPressed(); service.fromMPressed();
      service.recordPrToggled ();
      service.digitPressed (0);
      service.startPressed ();
      expect (service.registers).toEqual(theseRegisterValues({ reg_M: new Big(r) }));

    }
  });

  it ("should execute a simple program correctly", () => {
    const program_str = "A,V\n,S\n,toA\na,fromM\nd,fromM\n,div\nR,toA\n/,V\n,toA\nA,div\n,W\na,V\nA,sub\nA,W\nA,print\n/,print\n,V";
    const program = parse_program (program_str);
    service.program_code = program;
    service.numberOfDecimalsChanged (0);
    service.labelPressed ('V');

    for (let j=0; j<10; ++j) { 
      const r = Math.floor (Math.random()*1000);
      const str_r = r.toFixed(0);
      for (let i=0; i<str_r.length; ++i) service.digitPressed (+str_r[i]);
      service.startPressed ();
      const mod = 1 - r%2;
      expect (service.registers).toEqual (theseRegisterValues({reg_A: mod}));
    }

  });

  it ("should execute a simple program step by step", () => {
    const program_str = "A,V\n,S\n,toA\na,fromM\nd,fromM\n,div\nR,toA\n/,V\n,toA\nA,div\n,W\na,V\nA,sub\nA,W\nA,print\n/,print\n,V";
    const program = parse_program (program_str);
    service.program_code = program;
    service.numberOfDecimalsChanged (0);
    
    service.runStep (); // unconditional jump destination AV, does nothing but start the machine 
    expect (service.running).toBe(true);

    for ( let j=0; j<10; ++j ) {

      const n = Math.floor (Math.random()*1000);
      const n_str = "" + n;

      service.runStep (); // S instruction: stops the machine
      expect (service.running).toBe(false);

      for (let i=0; i<n_str.length; ++i) service.digitPressed (+n_str[i]);
      service.runStep ();
      expect (service.running).toBe(true);
      expect (service.registers).toEqual (theseRegisterValues({reg_A: n, reg_M: n}));

      service.runStep ();
      expect (service.registers).toEqual (theseRegisterValues({reg_A: n, reg_M: 2}));

      service.runStep ();
      expect (service.registers).toEqual (theseRegisterValues({reg_A: Math.floor(n/2), reg_M: 2, reg_R: n%2}));
      
      service.runStep ();
      expect (service.registers).toEqual (theseRegisterValues({reg_A: n%2, reg_M: 2, reg_R: n%2}));

      service.runStep (); // conditional jump instruction /V, only updates the instruction pointer
      expect (service.registers).toEqual (theseRegisterValues({reg_A: n%2, reg_M: 2, reg_R: n%2}));

      if ( n%2 == 0 ) {
        service.runStep ();
        expect (service.registers).toEqual (theseRegisterValues({reg_A: 2, reg_M: 2, reg_R: n%2}));
          
        service.runStep ();
        expect (service.registers).toEqual (theseRegisterValues({reg_A: 1, reg_M: 2, reg_R: 0}));
        
        service.runStep (); // unconditional jump to W
        expect (service.registers).toEqual (theseRegisterValues({reg_A: 1, reg_M: 2, reg_R: 0}));  
      }
      else {      
        service.runStep ();
        expect (service.registers).toEqual (theseRegisterValues({reg_A: 0, reg_M: 1, reg_R: 0}));

        // label AW, should not change the registers
        service.runStep ();
        expect (service.registers).toEqual (theseRegisterValues({reg_A: 1-n%2}));
      }

      service.runStep (); // A print
      expect (service.registers).toEqual (theseRegisterValues({reg_A: 1-n%2}));
  
      service.runStep (); // / print
      expect (service.registers).toEqual (theseRegisterValues({reg_A: 1-n%2}));

      service.runStep (); // unconditional jump instruction V
      expect (service.registers).toEqual (theseRegisterValues({reg_A: 1-n%2}));
    
    }

  });

});
