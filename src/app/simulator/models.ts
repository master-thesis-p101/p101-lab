import {Observable} from 'rxjs';
import {Big} from 'big.js';

export type FullRegister = 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'R' | 'M';
export type HalfRefister = 'a' | 'b' | 'c' | 'd' | 'e' | 'f';
// TODO: maybe remove '', keep only 'M' and handle this case only when printing
// register r is needed as temporary value for the RS instruction and for constant coding
export type SpecialRegister = '' | 'r' | '/';
export type Register = FullRegister | HalfRefister | SpecialRegister;

// TODO: maybe remove the `reg_` and use Register as key for the index signature for a stricter type check (?)
export type RegisterValues = {
    reg_M: Big;
    reg_A: Big;
    reg_R: Big | null;
    reg_r: Big | null
    reg_B: Big | null;
    reg_b: Big | null;
    reg_C: Big | null;
    reg_c: Big | null;
    reg_D: Big | null;
    reg_d: Big | null;
    reg_E: Big | null;
    reg_e: Big | null;
    reg_F: Big | null;
    reg_f: Big | null;

    [reg: string]: Big | null;
};

export type MemoryOperator = 'fromM' | 'toA' | 'exchange';
export type Label = 'V' | 'W' | 'Y' | 'Z';
export type ArithmeticOperator = 'sqrt' | '+' | '-' | 'x' | ':';
export type ServiceOperator =  'print'  | '*' | 'S'; 
export type Operator = MemoryOperator | Label | ArithmeticOperator | ServiceOperator;

export class MachineStatus {
    
    //registers
    registers: RegisterValues;
    
    // internal flags (from the original simulator code)
    signM: 1 | -1;
    virgolaPresente: boolean;
    slashPressed: boolean;
    selectedRegister: Register;
    resetM: boolean;
    instructionCounter: number;
    currentInstruction: number | null;
    running: boolean;
    blocked: boolean;
    clearLastOnce: boolean;
    memoryToClear: boolean;
    partial: boolean;
    constantInstructionNumber: number;
    
    // assets from the original simulator code
    recordPrPressed: boolean;
    printPrPressed: boolean;
    keybRlPressed: boolean;
    numberOfDecimals: number;

    // more flags to fix some weird stuff in the original simulator code (mixing strings and numbers...)
    putDecimalSeparatorBeforeNextDigit: boolean;
    decimalMultiplier: Big;

}

export interface Instruction {
    register: Register,
    operator: Operator,
};

// this is needed for the type-checking: reg.lowercase() is a string while halfRegisterMap[reg] is a Register and can be used as such.
// also, some registers ('/', 'M' and '') does not support splitting: for those registers halfRegisterMap[reg] is undefined
export const halfRegistersMap: {[reg: string]: HalfRefister | 'r' | '/'} = {
    'A': 'a',
    'B': 'b',
    'C': 'c',
    'D': 'd',
    'E': 'e',
    'F': 'f',
    'R': 'r',
    '' : '/',
}

// this is needed for the type-checking: reg.uppercase() is a string while wholeRegisterMap[reg] is a Register and can be used as such.
// also, some registers ('/', 'M' and '') does not support splitting: for those registers wholeRegisterMap[reg] is undefined
export const fullRegistersMap: {[reg: string]: FullRegister} = {
    'a': 'A',
    'b': 'B',
    'c': 'C',
    'd': 'D',
    'e': 'E',
    'f': 'F',
    'r': 'R',
}

/**
 * 
 * Classes that implement this interface are Programma101 simulators.
 * 
 * The methods *Pressed() have to be called from the outside every time a key is pressed. In the implementation of these methods the class should update their internal state.
 * There are also methods to inspect the simulator memory (registers values) and to get the printer output.
 * 
 * The programs can also be given in input to the simulator or read from it just like the original machine can read from or write to the magnetic cards.
 * 
 * Depending on the implementation of this interface the programs in the machine memory can be run synchronously, asynchronously (or both ways):
 * 
 * - Synchronous run is implemented by running the program code when the S key (or a label key) is pressed.
 * - Asynchronous run is implemented by just setting the needsToBeStarted flag when the S key (or a label key) is pressed.
 * 
 * This second option allows the user to run the program in a different thread. The execution can be started by calling the runProgram() function.
 * 
 */
export interface P101Simulator {

    /*
     *
     * functions to be called when a key is pressed 
     *
     */
    digitPressed (d: number): void;
    dotPressed (): void;
    algebraicSignPressed (): void
    startPressed (): void;
    clearPressed (): void;
    resetPressed (): void;
    registerPressed (r: Register): void;
    clearRegisterPressed (): void;
    arithmeticOperatorPressed (o: ArithmeticOperator): void;    
    printPressed (): void;
    fromMPressed (): void;
    toAPressed (): void;
    exchangePressed (): void;
    labelPressed (label: Label): void;
    slashPressed (): void;

    /*
     *
     * functions to be called when an asset changes 
     *
     */
    recordPrToggled (): void;
    printPrToggled (): void;
    numberOfDecimalChanged (n: number): void;

    /**
     * returns the machine status
     */
    machineStatus: MachineStatus;

    /**
     * 
     * returns the printer output.
     * Only the lines printed after the last get are returned
     * 
     * */ 
    printerOutput: string[];

    /**
     * 
     * to be called to load a program into the machine's memory
     * 
     * @param card program to load
     * 
     */
    loadMagneticCard (card: Instruction[]): void;
 
    /**
     * 
     * set the internal status of the simulator (MachineStatus, error and program).
     * 
     */
    setInternalStatus (status: MachineStatus, err: string | null, program?: Instruction[]): void;

    /**
     * 
     * to be called to get the program from the machine's memory
     * 
     */
    writeMagneticCard (): Instruction[];

    /**
     * 
     * runs a step of the program starting from the current instruction, sets the running flag
     * 
     */
    runStep (): void;

    /**
     * 
     * runs one step of the program starting from the specified label, sets the running flag
     * 
     */
    runStepFromLabel (l: Label): void;

    /**
     * 
     * runs the program in the machine's memory, possibly executing only a certain number of instructions.
     * 
     * returns when the machine blocks (due to an error or an S instruction) or after executing the first batch_size instructions, whichever comes first.
     * The first time this function is called the execution starts from the beginning, the next times the execution starts from the instruction it was left off
     * 
     * @param batch_size maximum number of instruction to execute before stopping. If batch_size is missing or not a positive number the executed instructions are not limited.
     * 
     * @returns true if and only if the machine has not finished running the program (i.e. the machine executed exactly batch_size instructions without errors and no S instructions).
     * 
     */
    runProgram (batch_size?: number): boolean;

    /**
     * 
     * error!==null if and only if there was an error. The actual value is the description of the error. 
     * 
     */
    error: string | null;

    /**
     * 
     * running is true if and only if the machine is executing a program.
     * 
     */
    running: boolean;

    /**
     * 
     * the selected register or null if none selected
     * 
     */
    selectedRegister: Register | null;
    
    /**
     * 
     * the last arithmetic operator used or null if the last instruction was not an arithmetic instruction 
     * 
     */
    lastArithmeticOperator: ArithmeticOperator | null;
    
    /**
     * 
     * the string to be appended the register M in order to match its correct representation.
     * The correct representation of M may differ from the numeric representation if the last key pressed are the decimal dot or zero.
     * 
     * e.g. the user pressed the keys 1 . 2 0 0
     *      the numeric value of M is 1.2
     *      the "correct" representation of the value of M should be 1.200
     *      therfore pendingStringRegisterM is the string "00"
     * 
     * e.g. the user pressed the keys 1 3 .
     *      the numeric value of M is 13
     *      the "correct" representation of the value of M should be 13.
     *      therfore pendingStringRegisterM is the string "."
     * 
     * e.g. the user pressed the keys 1 3 . 0 4 
     *      the numeric value of M is 13.04
     *      the "correct" representation of the value of M should be 13.04
     *      therfore pendingStringRegisterM is the empty string
     * 
     */
    pendingStringRegisterM: string;

    /**
     * 
     * emits each time the values of one or more registers change.
     * 
     */
    registersChanged$: Observable<{[reg: string]: {old_value: Big | null, new_value: Big | null}}>;

    /**
     * 
     * true if and only if the machine has to be started. It can be true after the S key or a label key (V,W,Y,Z) is pressed
     * 
     */
    needsToBeStarted: boolean;
}
