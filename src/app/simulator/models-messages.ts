
import {Instruction, MachineStatus, P101Simulator, Register} from './models';
import { Big } from 'big.js';

interface SerialMachineStatus {

    registers: {
        reg_M: string;
        reg_A: string;
        reg_R: string | null;
        reg_r: string | null
        reg_B: string | null;
        reg_b: string | null;
        reg_C: string | null;
        reg_c: string | null;
        reg_D: string | null;
        reg_d: string | null;
        reg_E: string | null;
        reg_e: string | null;
        reg_F: string | null;
        reg_f: string | null;
    };

    signM: 1 | -1;
    virgolaPresente: boolean;
    slashPressed: boolean;
    selectedRegister: Register;
    resetM: boolean;
    instructionCounter: number;
    currentInstruction: number | null;
    running: boolean;
    blocked: boolean;
    clearLastOnce: boolean;
    memoryToClear: boolean;
    partial: boolean;
    constantInstructionNumber: number;
    
    recordPrPressed: boolean;
    printPrPressed: boolean;
    keybRlPressed: boolean;
    numberOfDecimals: number;

    putDecimalSeparatorBeforeNextDigit: boolean;
    decimalMultiplier: string;
}

export interface MessageFromSimulatorWorkerToService {
    status: SerialMachineStatus;
    error: string | null;
    output: string[];
}

export interface MessageFromServiceToSimulatorWorker {
    program: Instruction[];
    status: SerialMachineStatus;
}

function serialize_machine_status (ms: MachineStatus): SerialMachineStatus {
    return {
        ...ms,
        decimalMultiplier: ms.decimalMultiplier.toString(),
        registers: {
            reg_M: ms.registers.reg_M.toString(),
            reg_A: ms.registers.reg_A.toString(),
            reg_R: ms.registers.reg_R ? ms.registers.reg_R.toString() : null,
            reg_r: ms.registers.reg_r ? ms.registers.reg_r.toString() : null,
            reg_B: ms.registers.reg_B ? ms.registers.reg_B.toString() : null,
            reg_b: ms.registers.reg_b ? ms.registers.reg_b.toString() : null,
            reg_C: ms.registers.reg_C ? ms.registers.reg_C.toString() : null,
            reg_c: ms.registers.reg_c ? ms.registers.reg_c.toString() : null,
            reg_D: ms.registers.reg_D ? ms.registers.reg_D.toString() : null,
            reg_d: ms.registers.reg_d ? ms.registers.reg_d.toString() : null,
            reg_E: ms.registers.reg_E ? ms.registers.reg_E.toString() : null,
            reg_e: ms.registers.reg_e ? ms.registers.reg_e.toString() : null,
            reg_F: ms.registers.reg_F ? ms.registers.reg_F.toString() : null,
            reg_f: ms.registers.reg_f ? ms.registers.reg_f.toString() : null,
        }
    }
}

export function deserializaMachineStatus (ms: SerialMachineStatus): MachineStatus {
    return {
        ...ms,
        decimalMultiplier: new Big (ms.decimalMultiplier),
        registers: {
            reg_M: new Big(ms.registers.reg_M),
            reg_A: new Big(ms.registers.reg_A),
            reg_R: ms.registers.reg_R ? new Big(ms.registers.reg_R) : null,
            reg_r: ms.registers.reg_r ? new Big(ms.registers.reg_r) : null,
            reg_B: ms.registers.reg_B ? new Big(ms.registers.reg_B) : null,
            reg_b: ms.registers.reg_b ? new Big(ms.registers.reg_b) : null,
            reg_C: ms.registers.reg_C ? new Big(ms.registers.reg_C) : null,
            reg_c: ms.registers.reg_c ? new Big(ms.registers.reg_c) : null,
            reg_D: ms.registers.reg_D ? new Big(ms.registers.reg_D) : null,
            reg_d: ms.registers.reg_d ? new Big(ms.registers.reg_d) : null,
            reg_E: ms.registers.reg_E ? new Big(ms.registers.reg_E) : null,
            reg_e: ms.registers.reg_e ? new Big(ms.registers.reg_e) : null,
            reg_F: ms.registers.reg_F ? new Big(ms.registers.reg_F) : null,
            reg_f: ms.registers.reg_f ? new Big(ms.registers.reg_f) : null,
        }
    }
}
  
export function createMessageForWorker ( s: P101Simulator ): MessageFromServiceToSimulatorWorker {
    return {
        program: s.writeMagneticCard(),
        status: serialize_machine_status (s.machineStatus),
    }
}

export function createMessageForService ( s: P101Simulator ): MessageFromSimulatorWorkerToService {
    return {
        status: serialize_machine_status (s.machineStatus),
        error: s.error,
        output: s.printerOutput
    }
}
