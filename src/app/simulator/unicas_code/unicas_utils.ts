/*
    based on the original p101 simulator developed at the University of Cassino.
    
    http://p101.unicas.it/js/funzioni.js
    http://p101.unicas.it/js/reader.js
    http://p101.unicas.it/js/main.js

    The original source code has no license attached, so it must be assumed that all rights are reserved.
    I asked the authors if I could reuse, modify, and redistribute their code under an open source license and they agreed.

*/

import {halfRegistersMap, Instruction, Operator, Register} from '../models';
import {Big} from 'big.js';

export function isConstantAsInstruction(i: Instruction){
    return i.register == 'a' && i.operator == 'fromM';
}

export function isJumpInstruction(i: Instruction){
    return i.operator == 'V' || i.operator == 'W' || i.operator == 'Y' || i.operator == 'Z';
}

export function isHalfRegister (r: Register) {
    return r == 'a' || r == 'b' || r ==  'c' || r == 'd' || r == 'e' || r == 'f';
}

 /** returns the digit corresponding to an operator in constant encoding */
 function operator_to_digit ( op: Operator ) {
    switch (op) {
        case 'S': return 0;
        case 'toA': return 1;
        case 'fromM': return 2;
        case 'exchange': return 3;
        case '+': return 4;
        case '-': return 5;
        case 'x': return 6;
        case ':': return 7;
        case 'print': return 8;
        case '*': return 9;
        default: throw new Error ("wrong operator in operator_to_digit: "+op);
    }
}

/**
 * decodes a constant from an array of instructions, starting from a specified index and using any following instructions needed
 * returns the constant and the index of the last instruction that encodes it.
 * 
 * @param instructions array of instruction
 * @param start index where the constant starts (index of the <a, fromM> instruction)
 */
export function decode_constant (instructions: Instruction[], start: number) {

    let instruction = instructions[start];
    let out = "";
    let sign = 0;
    let is_dot = false;

    if (instruction.operator != 'fromM' && instruction.register != 'a') throw new Error ("(BUG) decode_constant must be called starting from the <a, fromM> instruction");
    // skip the a fromM instruction
    ++start;

    instruction = instructions[start];
    while (instruction.register != 'D' && instruction.register != 'd' && instruction.register != 'E' && instruction.register != 'e') {
        if (sign >= 0 && (instruction.register == 'R' || instruction.register == 'r')) sign = 1;
        else if (sign <= 0 && (instruction.register == 'F' || instruction.register == 'f')) sign = -1;
        else throw new Error ("Wrong instruction encoding: sign changed at instruction <"+ instruction.register +"," + instruction.operator + ">");

        if (instruction.register == 'r' || instruction.register == 'f'){
            if (!is_dot) {
                out = '.' + out;
            }
            else {
                throw new Error ("Wrong instruction encoding: double dot at instruction <"+ instruction.register +"," + instruction.operator + ">");
            }
        }

        const d = operator_to_digit (instruction.operator);
        out = d + out;

        ++start;
        instruction = instructions[start];
    }
    if (sign >= 0 && (instruction.register == 'D' || instruction.register == 'd')) sign = 1;
    else if (sign <= 0 && (instruction.register == 'E' || instruction.register == 'e')) sign = -1;
    else throw new Error ("Wrong instruction encoding: sign changed at instruction <"+ instruction.register +"," + instruction.operator + ">");

    if (instruction.register == 'd' || instruction.register == 'e'){
        if (!is_dot) {
            out = '.' + out;
        }
        else {
            throw new Error ("Wrong instruction encoding: double dot at instruction <"+ instruction.register +"," + instruction.operator + ">");
        }
    }

    const d = operator_to_digit (instruction.operator);
    out = d + out;

    return {
        constant: new Big(out).mul (sign),
        index: start,
    }
}

function digit_to_operator (value: number): Operator {
    switch (value) {
        case 0:
            return "S";
        case 1:
            return "toA";
        case 2:
            return "fromM";
        case 3:
            return "exchange";
        case 4:
            return "+";
        case 5:
            return "-";
        case 6:
            return "x";
        case 7:
            return ":";
        case 8:
            return "print";
        case 9:
            return "*";
        default:
            throw new Error ("Expected value to be a digit in constantInstruction, got " + value);
    }
};

/**
 * encode a constant using one or more instructions and returns them
 * @param constant the constant to encode
 */
export function encode_constant ( constant: Big ) {
    const out: Instruction[] = []
    const instruction: Instruction = {operator: "fromM", register:"a"};
    out.push (instruction);
    let numero = constant;
    const registro_base = numero.gt(0) ? 'R' : 'F';
    numero = numero.abs();
    let stringa = numero.toString();
    let n_cifre = stringa.length;

    let index = n_cifre - 1;
    const pos_virgola = stringa.indexOf('.');
    if (pos_virgola !== -1) {   //controllo virgola e segno posizione
        index = stringa.indexOf('.') - 1;
        stringa = stringa.replace('.', '');  //rimuovo la virgola
        n_cifre = stringa.length;
    }

    for (let i = n_cifre - 1; i >= 0; i--) {  //scorro la stringa
        const value = stringa[i];
        const operator = digit_to_operator(+value);
        let registro: Register = registro_base;
        if (i == 0) {   //assegno D o E alla cifra più significativa
            if (registro == 'R') {
                registro = 'D';
            }
            else {
                registro = 'E';
            }
        }
        if (i == index) {     //aggiungo / dove è presente la virgola
            registro = halfRegistersMap[registro];
            if (!registro) throw new Error ("bug in codificaCostanti: expected registro to be a full register, got "+registro);
        }
        
        const coding_instruction = {operator, register: registro};
        out.push (coding_instruction);
    }
    return out;
}
