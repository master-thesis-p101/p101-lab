import { ArithmeticOperator, Instruction, Label, MachineStatus, P101Simulator, Register } from "@app/simulator";
import { Subject } from "rxjs";
import { OriginalSimulatorCodeWrapper } from "./original_code_wrapper";

import {Big} from 'big.js';

export class UnicasSimulator extends OriginalSimulatorCodeWrapper implements P101Simulator {

    private _last_operator_pressed: ArithmeticOperator | null;
    private _pending_string = "";
    
    private _registers_changed$ = new Subject <{[reg: string]: {old_value: Big | null, new_value: Big | null}}> ();

    constructor (async=false) {
        super(async);
    }

    // DEBUG AND CHANGE DETECTION UTILITIES
    private _previous_machine_status: MachineStatus;
    private _before_unicas_function ( fun_name: string, ...args: any[] ) {
        const label = args ? "[UnicasSimulator] "+fun_name+"("+args+")" : "[UnicasSimulator] "+fun_name+"()";
        console.group (label);
        this._previous_machine_status = { ...this.statoMacchina, registers: {...this.statoMacchina.registers} };
        console.debug ("[UnicasSimulator]   machine status before", this._previous_machine_status);
    }
    private _after_unicas_function () {
        console.debug ("[UnicasSimulator]   machine status after", this.statoMacchina);
        console.debug ("[UnicasSimulator]   changed:");
        const register_old_and_new_values: {[reg: string]: {old_value: Big | null, new_value: Big | null}} = {};
        let any_changed = false;
        Object.keys (this.statoMacchina).forEach (k => {
            if (k!=='registers' && this._previous_machine_status[k] !== this.statoMacchina[k]) {
                console.debug ("[UnicasSimulator]   ",k,this._previous_machine_status[k], " -> ", this.statoMacchina[k]);
                any_changed = true;
            }
        });
        const prev_reg = this._previous_machine_status.registers;
        const curr_reg = this.statoMacchina.registers;
        Object.keys (curr_reg).forEach (k => {
            if (curr_reg[k] !== prev_reg[k]) {
                const pv = prev_reg[k];
                const cv = curr_reg[k];
                const prev_v = pv ? pv.toString() : null;
                const curr_v = cv ? cv.toString() : null;
                console.debug ("[UnicasSimulator]   ",k,prev_v, " -> ", curr_v);
                register_old_and_new_values[k] = {old_value: pv, new_value: cv};
                any_changed = true;
            }
        });
        if (!any_changed) {
            console.debug ("[UnicasSimulator]   (none changed)");
        }
        if (this.statoMacchina.instructionCounter != 0) {
            console.debug ("[UnicasSimulator]   memory:", this.memory.slice(0, this.statoMacchina.instructionCounter));
        }
        console.groupEnd ();
        if (Object.keys(register_old_and_new_values).length) {
            this._registers_changed$.next (register_old_and_new_values);
        }
    }

    digitPressed (d: number) {
        this._before_unicas_function ("digitPressed", d);
        if (this.statoMacchina.resetM || d!=0) {
            this._pending_string = '';
        }
        else if ( d==0 ) {
            if (this.statoMacchina.putDecimalSeparatorBeforeNextDigit) {
                this._pending_string = ".0";
            }
            else if (this.statoMacchina.virgolaPresente) {
                this._pending_string += '0';
            }
        }
        super.unlock ();
        super.input ('' + d);
        this._after_unicas_function ();
    }

    dotPressed () {
        this._before_unicas_function ("dotPressed");
        super.unlock ();
        super.inserisciVirgola()
        this._after_unicas_function ();
    }

    algebraicSignPressed () {
        this._before_unicas_function ("algebraicSignPressed");
        super.unlock ();
        super.cambiaSegno ();
        this._after_unicas_function ();
    }

    startPressed () {
        this._before_unicas_function ("startPressed");
        super.unlock ();
        super.start ();
        this._after_unicas_function ();
    }

    clearPressed () {
        this._before_unicas_function ("clearPressed");
        super.unlock ();
        super.clear ();
        this._after_unicas_function ();
    }

    resetPressed () {
        this._before_unicas_function ("resetPressed");
        super.resetFunction (false);
        this._after_unicas_function ();
    }

    registerPressed (r: Register) {
        this._before_unicas_function ("registerPressed", r);
        super.unlock ();
        super.selectRegister (r);
        this._after_unicas_function ();
    }

    clearRegisterPressed () {
        this._before_unicas_function ("clearRegisterPressed");
        super.unlock ();
        super.clearRegister ();
        this._after_unicas_function ();
    }

    arithmeticOperatorPressed (o: ArithmeticOperator) {
        this._before_unicas_function ("aritmethicOperatorPressed", o);
        super.unlock ();
        switch (o) {
            case '+':
                super.somma ();
                break;
            case '-':
                super.sottrai ();
                break;
            case ':':
                super.dividi ();
                break;
            case 'x':
                super.moltiplica ();
                break;
            case 'sqrt':
                super.radice ();
                break;
        }
        this._last_operator_pressed = o;
        this._after_unicas_function ();
    }

    printPressed () {
        this._before_unicas_function ("printPressed");
        super.unlock ();
        super.print ();
        this._after_unicas_function ();
    }

    fromMPressed () {
        this._before_unicas_function ("fromMPressed");
        super.unlock ();
        super.fromM ();
        this._after_unicas_function ();
    }

    toAPressed () {
        this._before_unicas_function ("toAPressed");
        super.unlock ();
        super.toA ();
        this._after_unicas_function ();
    }

    exchangePressed () {
        this._before_unicas_function ("exchangePressed");
        super.unlock ();
        super.exchange();
        this._after_unicas_function ();
    }

    recordPrToggled () {
        this._before_unicas_function ("recordPrPressed");
        super.recordPR_Update ();
        this._after_unicas_function ();
    }
    
    printPrToggled () {
        this._before_unicas_function ("printPrPressed");
        super.printPR_Update ();   
        this._after_unicas_function ();
    }

    labelPressed (l: Label) {
        this._before_unicas_function ("labelPressed", l);
        super.unlock ();
        super.jumpInstruction (l);
        this._after_unicas_function ();
    }

    slashPressed () {
        this._before_unicas_function ("slashPressed");
        super.unlock ();
        super.slash ();   
        this._after_unicas_function ();
    }

    numberOfDecimalChanged (n: number) {
        this._before_unicas_function ("numberOfDecimalChanged");
        super.cambiaDecimali (n);
        this._after_unicas_function ();
    }

    runStep () {
        this._before_unicas_function ("runStep");
        super.unlock ();
        super.runStep ();
        this._after_unicas_function ();
    }
    
    runStepFromLabel (l: Label) {
        this._before_unicas_function ("runStepFromLabel", l);
        super.unlock ();
        super.runStepFromLabel (l);
        this._after_unicas_function ();
    }

    runProgram ( batch_size?: number ) {
        this._before_unicas_function ("runProgramBatched");
        super.unlock ();
        const t = super.runProgramBatched (batch_size);
        this._after_unicas_function ();
        return t;
    }

    get printerOutput () {
        const out = this.printer_lines;
        this.printer_lines = [];
        return out;
    }

    loadMagneticCard (card: Instruction[]) {
        if (this.statoMacchina.recordPrPressed) {
            console.warn ("[UnicasSimulator] Possible bug: trying to load magnetic card while RECORD PR is pressed");
            return;
        }
        this._before_unicas_function ("loadMagneticCard", card.length + " instructions");
        if (this.statoMacchina.printPrPressed) {
            console.debug("[UnicasSimulator] load partial card");
            if (card.length > 48) {
                this.statoMacchina.blocked = true;
                this._error = "Una cartolina parziale può contenere al massimo 48 istruzioni";
                return;
            }
            this.statoMacchina.registers.reg_F = null;
            this.statoMacchina.registers.reg_f = null;
            this.statoMacchina.instructionCounter = 72;
        }
        else {
            console.debug("[UnicasSimulator] load full magnetic card");
            if (card.length > 120) {
                this.statoMacchina.blocked = true;
                this._error = "Una cartolina magnetica può contenere al massimo 120 istruzioni";
                return;
            }
            this.statoMacchina.instructionCounter = 0;
        }
        this.statoMacchina.registers.reg_D = null;
        this.statoMacchina.registers.reg_d = null;
        this.statoMacchina.registers.reg_E = null;
        this.statoMacchina.registers.reg_e = null;
        for (let i=0; i<card.length; ++i, ++this.statoMacchina.instructionCounter) {
            this.memory[this.statoMacchina.instructionCounter] = card[i];
        }
        // TODO: implement writing constants to DEF, then decomment
        //super.searchConst();
        this.statoMacchina.currentInstruction = 0;
        this._after_unicas_function ();
    }

    writeMagneticCard () {
        return this.memory.slice (0, this.statoMacchina.instructionCounter);
    }

    setInternalStatus ( status: MachineStatus, err: string | null, program?: Instruction[] ) {
        this.statoMacchina = status;
        this._error = err;
        if (program !== undefined) this.memory = program;
    }

    get error () {
        return this._error;
    }

    get running () {
        return this.statoMacchina.running;
    }

    get machineStatus () {
        return this.statoMacchina;
    }

    get selectedRegister () {
        return this.statoMacchina.selectedRegister;
    }

    get lastArithmeticOperator () {
        return this._last_operator_pressed;
    }

    get pendingStringRegisterM () {
        if (this.statoMacchina.putDecimalSeparatorBeforeNextDigit) {
            return '.';
        }
        return this._pending_string;
    }

    get registersChanged$ () {
        return this._registers_changed$.asObservable();
    }

    get needsToBeStarted () {
        return this.needs_to_be_started;
    }

}
