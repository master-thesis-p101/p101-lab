/*
    based on the original p101 simulator developed at the University of Cassino: http://p101.unicas.it/js/funzioni.js

    The original source code has not any license attached, so it must be assumed that all rights are reserved.
    I asked them if I could reuse, modify, and redistribute their code and they replied "sure, go on".

*/

import { Subject } from "rxjs";
import { Instruction, MachineStatus } from "@app/simulator";

const _INTERNAL_MEMORY_SIZE = 120;

/**
 * 
 * class UnicasHelperBase: base class used to integrate the original simulator code, which relies on the existence of global variables.
 * All the global variables are stored as members of UnicasHelperBase; each original source file is turned into a class which inherits from UnicasHelperBase and exposes the functions needed for the simulator to work.
 * 
 */
export class UnicasHelperBase {

    protected statoMacchina: MachineStatus;

    protected _error: string|null;

    protected printer_lines: string[] = [];

    protected memory = new Array<Instruction> (_INTERNAL_MEMORY_SIZE);

    // if true the program has to be run asyncronously
    protected needs_to_be_started = false;
    
    protected _clear_memory () {
        this.memory = new Array<Instruction> (_INTERNAL_MEMORY_SIZE);
        this.statoMacchina.instructionCounter = 0;
    }

    protected stampa (line: string) {
        console.debug ("[UnicasHelperBase]    prining line", line);
        this.printer_lines.push (line);
    }


}
