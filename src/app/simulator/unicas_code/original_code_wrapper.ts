/*
    based on the original p101 simulator developed at the University of Cassino.
    
    http://p101.unicas.it/js/funzioni.js
    http://p101.unicas.it/js/reader.js
    http://p101.unicas.it/js/main.js

    The original source code has no license attached, so it must be assumed that all rights are reserved.
    I asked the authors if I could reuse, modify, and redistribute their code under an open source license and they agreed.

*/

//TODO: maybe specialize types a little bit (Label instead of Operator and so on)

import {UnicasHelperBase} from './unicas_base_class_for_integration';
import {decode_constant, encode_constant, isConstantAsInstruction, isHalfRegister, isJumpInstruction} from './unicas_utils';

import {halfRegistersMap, Instruction, Operator, Register} from '@app/simulator';
import {Big} from 'big.js';
import { Label } from '../models';

Big.NE = -16;
Big.RM = 0;

export class OriginalSimulatorCodeWrapper extends UnicasHelperBase {

    constructor ( private _async = false ) {
        super ();
        this.resetFunction ();
    }

    //Controllo se registro è disponibile:
    private isAvailable(letter: string | null) {
        if (letter == null) {
            return true
        }
        if (letter != letter.toUpperCase()) {
            let upperRegister = letter.toUpperCase();
            upperRegister = "reg_" + upperRegister;
            const value = this.statoMacchina.registers[upperRegister] || new Big(0);
            let string_value = value.toString();
            string_value = string_value.replace('.', '');
            string_value = string_value.replace('-', '');
            const value_length = string_value.length;

            if (value_length > 11) {
                this._error = "Il registro "+letter.toUpperCase()+" non può essere diviso perché contiene un valore con più di undici cifre";
                this.statoMacchina.blocked = true;
                return false
            }
            else {
                return true
            }
        }
        else
            return true
    }
    
    private storeInRegister (value: Big, destination: Register) {
        // controllare che nei splittati (quindi maiuscoli e minuscoli) le cifre massime son 11
        console.log('Memorizzo ', value.toString(), ' nel registro ', destination);
        if (!this.isAvailable(destination)) {
            return
        }
        const MAX_VALUE_22 = 9999999999999999999999;
        const MAX_VALUE_11 = 99999999999;

        if (value.gt (MAX_VALUE_22) || value.lt(-MAX_VALUE_22) || value.toString().length > 22) {
            console.log("Errore, un registro può contenere al più 22 cifre");
            this._error = "un registro può contenere al più 22 cifre";
            this.statoMacchina.blocked = true;
            return
        }
        // direct storage to a splitted register
        const value_str = value.toString().replace('.', '').replace('-', '');
        const value_length = value_str.length;

        if (value.gt(MAX_VALUE_11) || value.lt(-MAX_VALUE_11) || value_length > 11) {
            const splitted = "reg_" + destination.toLowerCase();
            if (isHalfRegister(destination)) {
                this._error = "overload: il valore da inserire nel registro è troppo grande"
                this.statoMacchina.blocked = true;
                return
            }
            else if (destination != 'M' && destination != 'A' && destination != 'R' && this.statoMacchina.registers[splitted]!=null) {
                this._error = "overload: il registro è diviso e il valore da inserire è troppo grande";
                this.statoMacchina.blocked = true;
                return
            }
        }
        // we can store the value
        const dst = "reg_" + destination;
        if (destination == 'R'){
            value = new Big (this.toFixedWithTruncation_R(value));
        }
        this.statoMacchina.registers[dst] = value;
    }

    private _make_instruction ( operator: Operator, register?: Register ): Instruction {
        return {
            register: register || this.statoMacchina.selectedRegister || "",
            operator,
        };
    }

    protected print() {
        const letter = this.statoMacchina.selectedRegister;
        console.log('stampa registro:' + letter);
        //create the istruction

        let instruction: Instruction;
        if (this.statoMacchina.slashPressed && (letter == 'M' || !letter)) {
            instruction = this._make_instruction ('print', '/');
        }
        else {
            instruction = {
                register: this.statoMacchina.selectedRegister == null ? '' : this.statoMacchina.selectedRegister,
                operator: 'print',
            }
        }
        this.scegliMod(instruction);
    };

    protected start() {
        //start come istruzione

        // TODO: what to do if this.statoMacchina.running == true
        //       I set it to false to make this usecase work: runStep(); runStep(); start()
        //       change this if the original P101 stopped the running program instead
        this.statoMacchina.running = false;  

        const i: Instruction = {register: this.statoMacchina.selectedRegister == 'R' ? 'R' : '', operator: 'S'};
        this.scegliMod(i);
    }

    protected somma() {
        const instruction = this._make_instruction ('+');
        this.scegliMod(instruction);
    };

    protected sottrai() {
        const instruction = this._make_instruction ('-');
        this.scegliMod(instruction);
    };

    protected moltiplica() {
        const instruction = this._make_instruction ('x');
        this.scegliMod(instruction);

        if (!this.statoMacchina.recordPrPressed && !this.statoMacchina.printPrPressed && !this.statoMacchina.blocked) {
            this.printResult();
        }
    }

    protected dividi() {

        const instruction = this._make_instruction (':');
        this.scegliMod(instruction);

        if (!this.statoMacchina.recordPrPressed && !this.statoMacchina.printPrPressed && !this.statoMacchina.blocked) {
            this.printResult();
        }

    };

    protected radice() {
        const instruction = this._make_instruction ('sqrt');
        this.scegliMod(instruction);

        if (!this.statoMacchina.recordPrPressed && !this.statoMacchina.printPrPressed) {
            this.printResult();
        }
    };

    protected fromM() {
        if (this.statoMacchina.selectedRegister !== 'R' && this.statoMacchina.selectedRegister !== 'A' && this.statoMacchina.selectedRegister !== 'a') {
            const instruction = this._make_instruction ('fromM');
            this.scegliMod (instruction);
        }
        if (this.statoMacchina.selectedRegister == 'a') {
            const value = this.statoMacchina.registers.reg_M;
            this.codificaCostanti(value);
            this.statoMacchina.slashPressed = false;
        }
    };

    protected toA() {
        if (this.statoMacchina.selectedRegister !== 'A') {
            const instruction = this._make_instruction ('toA');
            this.scegliMod(instruction);
        }
    }

    protected exchange() {
        let instruction: Instruction;
        if (this.statoMacchina.slashPressed && this.statoMacchina.selectedRegister == 'M' || this.statoMacchina.slashPressed && this.statoMacchina.selectedRegister == null) {
            instruction = this._make_instruction ('exchange', '/');
        }
        else {
            instruction = this._make_instruction ('exchange');
        }
        this.scegliMod(instruction);
    };

    protected scegliMod(instruction: Instruction) {
        if (this.statoMacchina.recordPrPressed) {
            //record the instruction
            if (this.statoMacchina.memoryToClear) {
                this._clear_memory ();
                this.statoMacchina.memoryToClear = false;
            }
            if (this.statoMacchina.partial) {
                this.statoMacchina.instructionCounter = 73;
            }
            const n = this.statoMacchina.instructionCounter;
            this.memory[n] = instruction;
            this.statoMacchina.instructionCounter++;
            this.printInstruction(instruction);
            this.statoMacchina.clearLastOnce = true;
        }
        else if (this.statoMacchina.printPrPressed && instruction.operator != 'S') {
            //print the instruction
            this.printInstruction(instruction);
        }
        else {
            //execute the instruction
            this.execute(instruction);
        }
        this.statoMacchina.selectedRegister = '';
        this.statoMacchina.resetM = true;
        this.statoMacchina.slashPressed = false;
        this.statoMacchina.signM = 1;
    };

    protected input (num: string) {

        let M = this.statoMacchina.registers.reg_M;

        if (num === '-') {
            return;
        }
        if (num === '.' && this.statoMacchina.virgolaPresente) {
            console.log("virgola già presente");
            return;
        }
        else if (num === '.' && !this.statoMacchina.virgolaPresente) {
            console.log("inserisco la virgola alla fine");
            // TODO: what happend in the original machine if the decimal separator key is pressed twice?
            this.statoMacchina.putDecimalSeparatorBeforeNextDigit = true;
            return;
        }

        
        //funzione che reinizializza M se è stato premuto un tasto diverso da un numero
        if (this.statoMacchina.resetM) {
            M = new Big (0);
            this.statoMacchina.resetM = false;
            this.statoMacchina.virgolaPresente = false;
            this.statoMacchina.putDecimalSeparatorBeforeNextDigit = false;
            this.statoMacchina.decimalMultiplier = new Big (0.1);
        }

        // from now on, num is a digit
        
        const d = this.statoMacchina.signM * Number(num);
        if (isNaN(d)) throw new Error("bug: expecting num to be a digit in Funzioni.input, got" + num);

        if (this.statoMacchina.virgolaPresente || this.statoMacchina.putDecimalSeparatorBeforeNextDigit) {
            M = M.add (this.statoMacchina.decimalMultiplier.mul(d));
            this.statoMacchina.decimalMultiplier = this.statoMacchina.decimalMultiplier.mul(0.1);
            this.statoMacchina.putDecimalSeparatorBeforeNextDigit = false;
            this.statoMacchina.virgolaPresente = true;
        }
        else {
            M = M.mul(10).add(d);
        }

        this.storeInRegister(M, 'M');  //memorizzo il valore sullo stato della macchina
    };

    protected selectRegister(letter: Register) {
        this.statoMacchina.selectedRegister = letter;
    };

    protected slash() {
        this.statoMacchina.slashPressed = true;
        const sr = this.statoMacchina.selectedRegister;

        if ( sr && halfRegistersMap[sr] !== undefined) {
            this.statoMacchina.selectedRegister = halfRegistersMap[sr];
        }
        console.log('selected ', this.statoMacchina.selectedRegister);
    };

    protected inserisciVirgola(){

        if (!this.statoMacchina.virgolaPresente) {
            this.statoMacchina.putDecimalSeparatorBeforeNextDigit = true;
        }
    };

    protected cambiaSegno() {
        if (this.statoMacchina.signM == 1) {
            this.statoMacchina.signM = -1;
            this.statoMacchina.registers.reg_M = this.statoMacchina.registers.reg_M.mul(-1);
        }
    }

    protected clear() {
        if (this.statoMacchina.recordPrPressed && this.statoMacchina.instructionCounter > 0 && this.statoMacchina.clearLastOnce) {
            console.log('clear while RECORD PR is pressed');
            //delete the instruction
            if (this.statoMacchina.constantInstructionNumber) {
                const counterEnd = this.statoMacchina.instructionCounter - this.statoMacchina.constantInstructionNumber - 1;
                console.log('Clear constant as instruction: ' + this.statoMacchina.constantInstructionNumber + 'istruzioni da rimuovere!');
                this.statoMacchina.instructionCounter = counterEnd;
                this.statoMacchina.constantInstructionNumber = 0;
            }
            else {
                this.statoMacchina.instructionCounter--;
            }

            this.stampa('');
            this.statoMacchina.selectedRegister = '';
            this.statoMacchina.resetM = false;
            this.statoMacchina.signM = 1;
            this.statoMacchina.registers.reg_M = new Big (0);
            this.statoMacchina.clearLastOnce = false;
        }
        else {
            //clear button
            console.log("Clear Button pressed");
            this.statoMacchina.registers.reg_M = new Big (0);
            this.statoMacchina.virgolaPresente = false;
            this.statoMacchina.selectedRegister = '';
            this.statoMacchina.resetM = false;
            this.statoMacchina.signM = 1;

        }
    };

    protected unlock() {
        if (this.statoMacchina.blocked) {
            this.statoMacchina.blocked = false;
            this._error = null;
        }
    };

    protected clearRegister() {
        const instruction = this._make_instruction ('*');
        this.scegliMod(instruction);
    };

    protected resetFunction( reset_decimals=true ) {
        const n_decimals = (reset_decimals || !this.statoMacchina) ? 2 : this.statoMacchina.numberOfDecimals;
        this.statoMacchina = {
            registers: {
                reg_M: new Big (0),
                reg_A: new Big (0),
                reg_R: null,
                reg_r: null,
                reg_B: null,
                reg_b: null,
                reg_C: null,
                reg_c: null,
                reg_D: null,
                reg_d: null,
                reg_E: null,
                reg_e: null,
                reg_F: null,
                reg_f: null,        
            },
            signM: 1,
            virgolaPresente: false,
            slashPressed: false,
            selectedRegister: '',
            resetM: false,
            instructionCounter: 0,
            currentInstruction: 0,
            running: false,
            blocked: false,
            clearLastOnce: false,
            memoryToClear: false,
            partial: false,
            constantInstructionNumber: 0,
            
            // assets from the original simulator code
            recordPrPressed: false,
            printPrPressed: false,
            keybRlPressed: false,
            numberOfDecimals: n_decimals,
        
            // more flags to fix some weird stuff in the original simulator code (mixing strings and numbers...)
            putDecimalSeparatorBeforeNextDigit: false,
            decimalMultiplier: new Big (0.1)
        };
        this._error = null;
        this._clear_memory ();
        this.needs_to_be_started = false;
    };

    private codificaCostanti(constant: Big) {
        console.log('codifica della costante:', constant.toString());
        const coding_instructions = encode_constant ( constant );
        for (let i=0; i<coding_instructions.length; ++i) {
            if (this.statoMacchina.recordPrPressed) {
                //record the instruction
                this.memory[this.statoMacchina.instructionCounter] = coding_instructions[i];
                this.statoMacchina.instructionCounter++;
            }
            if (this.statoMacchina.printPrPressed || this.statoMacchina.recordPrPressed) {
                this.printInstruction(coding_instructions[i]);
            }
        }
        this.statoMacchina.clearLastOnce = true;
        this.statoMacchina.constantInstructionNumber = coding_instructions.length-1;
        this.statoMacchina.selectedRegister = '';
        this.statoMacchina.resetM = true;
        this.statoMacchina.slashPressed = false;
        this.statoMacchina.signM = 1;
    };

    protected recordPR_Update(){
        if (this.statoMacchina.recordPrPressed == false)
        {
            console.log("Record Program mode ON");
            if (this.statoMacchina.printPrPressed == true)
            {
                this.statoMacchina.partial = true;
            }
            this._clear_memory ();
            this.statoMacchina.recordPrPressed = true;
            // TODO: implement writing constants to DEF, then decomment
            // this.verifyDEF();
        }
        else
        {
            console.log("Record Program mode OFF");
            this.statoMacchina.recordPrPressed = false;
            this.statoMacchina.memoryToClear = false;
            this.statoMacchina.partial = false;
        }
    }

    protected printPR_Update(){
        if (this.statoMacchina.printPrPressed == false) {
            console.log("Print Program mode ON");
            this.statoMacchina.printPrPressed = true;
        }
        else {
            this.statoMacchina.printPrPressed = false;
        }
    }

    private truncateDecimals(n: Big, n_digits: number) {
        let output_val = new Big(0);
        if (n && !n.eq(0)) {
            const output_str = n.toFixed(n_digits);
            output_val = new Big(output_str);
            // toFixed rounded up instead of down
            if (output_val > n) {
                output_val = output_val.sub (new Big(10).pow (-n_digits));
            } 
        }
        return output_val;
    };

    private toFixedWithTruncation_R (n: Big){
        return this.toFixedWithTruncation(n, 20);
    }

    private toFixedWithTruncation(n: Big, maxLen: number){
        let fixed = n.toFixed(maxLen);
        return fixed;
    }

    private addzeros(n: Big, n_digits: number) {
        let decimal = 0;
        const numberString = n.toString();
        const numberArray = numberString.split('.');
        let out = numberString;

        if (numberArray[1]) {
            decimal = numberArray[1].length;
        }
        if (decimal < n_digits) {
            const toappend = n_digits - decimal;
            if (numberArray[1]) {
                out = numberArray[0] + '.' + numberArray[1];
            }
            else {
                out = numberArray[0] + '.';
            }
            for (let i = 0; i < toappend; i++) {
                out = out + '0';
            }
        }
        return out;
    }

    private printProgram() {
        console.log('Stampa programma');
        this.stampa(''); //empty row

        for (let i = 0; i < this.statoMacchina.instructionCounter; i++) {
            const instruction = this.memory[i];

            if (instruction) {
                console.log('printprogram:', instruction);
                this.printInstruction(instruction);
            }
        }
    };

    private printInstruction(instruction: Instruction, constant?: Big) {
        if (this.statoMacchina.running) {
            return;
        }
        const sym = this.toSymbol(instruction.operator);
        this.stampa((constant!=null ? constant.toString() : '') + ' ' + (instruction.register || '') + ' ' + sym);
    };

    private toSymbol(op: Operator) {
        switch (op) {
            case "toA":
                return "↓";
            case "fromM":
                return "↑";
            case "exchange":
                return "↕";
            case "+":
                return "+";
            case "-":
                return "-";
            case "x":
                return "X";
            case ":":
                return "÷";
            case "print":
                return "◊";
            case "sqrt":
                return "√";
            default:
                return op;
        }
    };

    private mapJumpLabel(letter: Register): Register {
        switch (letter) {
            case 'C':
                return 'B';
            case 'c':
                return 'b';
            case 'D':
                return 'E';
            case 'd':
                return 'e';
            case 'R':
                return 'F';
            case 'r':
                return 'f';
            case null:
                return 'A';
            case '/':
                return 'a';
            default:
                return 'A';
        }
    }

    private printResult() {
        const value = this.statoMacchina.registers.reg_A;
        const value_str = this.addzeros(value, this.statoMacchina.numberOfDecimals);
        this.stampa(value_str + ' A ◊');
    };

    private verifyDEF() {
        if (this.statoMacchina.registers.reg_D) {
            this.storeConst('D');
        }
        if (this.statoMacchina.registers.reg_d) {
            this.storeConst('d');
        }
        if (this.statoMacchina.registers.reg_E) {
            this.storeConst('E');
        }
        if (this.statoMacchina.registers.reg_e) {
            this.storeConst('e');
        }
        if (this.statoMacchina.registers.reg_F) {
            this.storeConst('F');
        }
        if (this.statoMacchina.registers.reg_f) {
            this.storeConst('f');
        }
    };

    private storeConst(letter: Register) {
        const dst = "reg_" + letter;
        const value = this.statoMacchina.registers[dst];
        if (value == null) throw new Error ("(BUG) storeConst: value in register " + letter + " should be not null");
        // TODO: how are constants in D,E,F coded when writing in the magnetic cards?
        // const instruction = this._make_instruction ('store', letter);
        // this.memory[this.statoMacchina.instructionCounter] = instruction;
    };

    protected searchConst() {
        console.log('searching for store instruction ');
        for (let i = 0; i < this.statoMacchina.instructionCounter; i++) {
            const instruction = this.memory[i];
            if (instruction) {
                // TODO: how do the original P101 know which instructions are constants to store in D,E,F?
                // if (false) {
                //     console.log('store instruction found:', instruction);
                //     const value = this.decode_constant ();
                //     const destination = instruction.register;
                //     if (value==undefined || (destination != 'E' && destination != 'F') ) throw new Error ("wrong store instruction format " + instruction);
                //     this.storeInRegister(value, destination);
                // }
            }
        }
    };

    //Funzioni per ottenere numero di decimali e numero di interi di un float

    private int_num(value: Big) {
        let numberString = value.toString();
        numberString = numberString.replace('-', '');
        const numberArray = numberString.split('.');

        if (numberArray[0]) {
            return numberArray[0].length;
        }
        else {
            return 0;
        }
    }

    private dec_num(value: Big) {

        let numberString = value.toString();
        numberString = numberString.replace('-', '');
        const numberArray = numberString.split('.');

        if (numberArray[1]) {
            return numberArray[1].length;
        }
        else {
            return 0;
        }
    }
    
    private setEndProgram() {
        const counterLast = this.statoMacchina.instructionCounter;
        const counterPrevious = counterLast - 1;
        const previousInstruction = this.memory[counterPrevious];
        const lastInstruction = this.memory[counterLast];

        if (lastInstruction.operator != 'S') {
            console.log('Added two start instruction!')
            this.addStartInstruction();
            this.addStartInstruction();
        }
        if (previousInstruction.operator != 'S') {
            console.log('Added start instruction!')
            this.addStartInstruction();
        }
    }

    private addStartInstruction() {

        const instruction: Instruction = {register: '', operator: 'S'};
        this.memory[this.statoMacchina.instructionCounter] = instruction;
        this.statoMacchina.instructionCounter++;
    }

    protected jumpInstruction (label: Label) {
        if (this.statoMacchina.recordPrPressed) {
            //create the istruction
            let instruction: Instruction;
            const sr = this.statoMacchina.selectedRegister;
            if (!this.statoMacchina.slashPressed && (!sr || sr == 'M')) {
                instruction = {operator:label, register: ''};
            }
            else if (this.statoMacchina.slashPressed && (!sr || sr=="M")) {
                instruction = {operator:label, register: '/'};
                this.statoMacchina.slashPressed = false;
            }
            else {
                instruction = {operator:label, register: sr};
            }
            //record the instruction
            this.memory[this.statoMacchina.instructionCounter] = instruction;
            this.statoMacchina.instructionCounter++;
            this.statoMacchina.selectedRegister = '';
            this.printInstruction(instruction);
            this.statoMacchina.clearLastOnce = true;
        }
        else {
            if (this.statoMacchina.instructionCounter == 0) {
                if (this.statoMacchina.selectedRegister == null || this.statoMacchina.selectedRegister == 'C' || this.statoMacchina.selectedRegister == 'D' || this.statoMacchina.selectedRegister == 'R') {
                    this._error = 'Nessun programma caricato in memoria.';
                    this.statoMacchina.blocked = true;
                    return;
                }
                else if (this.statoMacchina.selectedRegister == 'c' || this.statoMacchina.selectedRegister == 'd' || this.statoMacchina.selectedRegister == 'r' || this.statoMacchina.selectedRegister == '/') {
                    this._error = 'Non puoi avviare un programma con una istruzione di salto condizionato.';
                    this.statoMacchina.blocked = true;
                    return;
                }
                else {
                    this.stampa(this.statoMacchina.selectedRegister + label);
                    this.statoMacchina.selectedRegister = '';
                    return;
                }
            }
            const dst = this.mapJumpLabel(this.statoMacchina.selectedRegister);
            this.seek(dst, label);
            if (!this.statoMacchina.resetM) {
                if (this.statoMacchina.selectedRegister != null && !this.statoMacchina.printPrPressed) {
                    this.stampa(this.statoMacchina.registers.reg_M + ' ' + this.statoMacchina.selectedRegister + label);
                }
                else {
                    if (!this.statoMacchina.printPrPressed) {
                        this.stampa(this.statoMacchina.registers.reg_M + ' ' + label);
                    }
                }
            }
            else {
                if (this.statoMacchina.selectedRegister != null && !this.statoMacchina.printPrPressed) {
                    this.stampa(this.statoMacchina.selectedRegister + label);
                }
                else {
                    if (!this.statoMacchina.printPrPressed) {
                        this.stampa(label);
                    }
                }
            }
            if (this.statoMacchina.printPrPressed) {
                this.printProgram();
                this.statoMacchina.resetM = true;
                return;
            }
            this.statoMacchina.selectedRegister = '';
            this.statoMacchina.resetM = true;
            this.statoMacchina.slashPressed = false;
            this.statoMacchina.signM = 1;
            if (!this.statoMacchina.blocked) {
                this.statoMacchina.running = true;
                if ( this._async ) {
                    this.needs_to_be_started = true;
                } 
                else {
                    this.runProgramBatched ();
                }
            }
        }
    }

    private execute (instruction: Instruction) {
        if (!instruction){
            this.statoMacchina.running = false;
            return;
        }
        if (this.statoMacchina.blocked){
            this.statoMacchina.running = false;
            return;
        }
       
        if(!isConstantAsInstruction(instruction) && !isJumpInstruction(instruction) && !this.isAvailable(instruction.register)){
            return;
        }
        
        console.log("executing", instruction);

        if (instruction.operator == 'S' && instruction.register != 'R'){
            if (this.statoMacchina.printPrPressed) {
                this.printProgram();
                return;
            }
            this.print_instruction_and_value_of_M(instruction);
            if (!this.statoMacchina.running){
                this.statoMacchina.running = true;
                console.log('starting the machine');
                if (this._async) {
                    this.needs_to_be_started = true;
                }
                else {
                    this.runProgramBatched ();
                }
            }
            else{
                this.statoMacchina.running = false;
                console.log('stopping the machine');
            }
        }
        if (instruction.operator == 'S' && instruction.register == 'R'){
            console.log('RS instruction');
            this.printInstruction(instruction);
            let tmp = this.statoMacchina.registers.reg_D;
            this.statoMacchina.registers.reg_D = this.statoMacchina.registers.reg_R;
            this.statoMacchina.registers.reg_R = tmp;
            tmp = this.statoMacchina.registers.reg_d;
            this.statoMacchina.registers.reg_d = this.statoMacchina.registers.reg_r;
            this.statoMacchina.registers.reg_r = tmp;	
        }
        if (instruction.operator == 'print') {
            console.log('print instruction');
            if (!instruction.register){
                instruction.register='M';
            }
            const dst = 'reg_' + instruction.register;
            let value = this.statoMacchina.registers[dst] || new Big(0);

            if (instruction.register != 'R'){
                value = this.truncateDecimals(value, this.statoMacchina.numberOfDecimals);
            }
            const value_to_print = this.addzeros(value, this.statoMacchina.numberOfDecimals);
            if (instruction.register == '/') {
                this.stampa('');
            }
            else if(instruction.register=='M'){
                this.stampa(value_to_print +'◊');
            }
            else{
                this.stampa(value_to_print + ' ' + instruction.register + ' ◊');
            }
        }

        if (isConstantAsInstruction(instruction)){

            console.log('Constant as instruction');
            this.printInstruction(instruction);
        
            const {constant, index} = decode_constant (this.memory, this.statoMacchina.currentInstruction || 0);
            this.statoMacchina.currentInstruction = index;

            this.storeInRegister(constant, 'M');
            
            if (this.statoMacchina.running == true){
                return;
            }   
        }
        if (instruction.register != 'a' && instruction.operator == 'fromM') {
            console.log('fromM command');
            this.print_instruction_and_value_of_M ( instruction );

            if (instruction.register == null || instruction.register == ''){
                instruction.register = 'M';
            }
            //constructing destination
            console.log("moving values from M to ", instruction.register);
            //storing the value
            this.storeInRegister(this.statoMacchina.registers.reg_M, instruction.register);
        }
        if (instruction.operator == 'toA') {
            console.log('toA instruction');
            this.print_instruction_and_value_of_M ( instruction );

            if (instruction.register == null || instruction.register == ''){
                instruction.register = 'M';
            }
            const source = instruction.register;
            console.log('moving values from ' + source + ' to A');
            const src = 'reg_' + source;
            const value =this.statoMacchina.registers[src] || new Big(0);
            this.storeInRegister(value, 'A');
        }
        
        if (instruction.operator == '+') {
            console.log('sum instruction');
            this._move_register_to_M (instruction);

            const float_M = this.statoMacchina.registers.reg_M;
            const float_A = this.statoMacchina.registers.reg_A;
            console.log('executing', float_A.toString() , '+' , float_M.toString());
            const R = float_A.add (float_M);
            const A = this.truncateDecimals(R, this.statoMacchina.numberOfDecimals); //valore troncato
            
            if (!this._check_registers_capacity (float_A, float_M)) return;

            this.storeInRegister(R, 'R');
            this.storeInRegister(A, 'A');
        }
    
        if (instruction.operator == '-') {
            console.log('subtract instruction');
            this._move_register_to_M (instruction);

            const float_M = this.statoMacchina.registers.reg_M;
            const float_A = this.statoMacchina.registers.reg_A;

            const R = float_A.sub(float_M);
            const A = this.truncateDecimals(R, this.statoMacchina.numberOfDecimals); //valore troncato

            if (!this._check_registers_capacity (float_A, float_M)) return

            this.storeInRegister(R, 'R');
            this.storeInRegister(A, 'A');
            
        }
        if (instruction.operator == 'x') {
            console.log('mult instruction');
            this._move_register_to_M (instruction);

            const float_M = this.statoMacchina.registers.reg_M || new Big(0);
            const float_A = this.statoMacchina.registers.reg_A || new Big(0);
            
            const R = float_M.mul(float_A);
            const A = this.truncateDecimals(R, this.statoMacchina.numberOfDecimals); //valore troncato
            
            if(this.int_num(float_A)+this.int_num(float_M)+this.dec_num(float_A)+this.dec_num(float_M) - 1 > 22 ||
            this.statoMacchina.numberOfDecimals > 22 - (this.int_num(float_A)+this.int_num(float_M)-1)){
                this.statoMacchina.blocked=true;
                this._error = "Questa operazione eccede la capacità dei registri";
                return
            }
            this.storeInRegister(R, 'R');
            this.storeInRegister(A, 'A');
            
        }
        if (instruction.operator == ':') {
            console.log('division operation');
            this._move_register_to_M (instruction);

            const float_M = this.statoMacchina.registers.reg_M || new Big(0);
            if (float_M.eq(0)){
                console.log("divisione per zero");
                this.statoMacchina.running = false;
                this.statoMacchina.blocked = true;
                this._error = "divisione per zero";
                return;
            }
            const float_A = this.statoMacchina.registers.reg_A;
            const quoziente = float_A.div(float_M);
            console.log(float_A.toString(),' / ',float_M.toString(),'=',quoziente.toString());
            const result = this.truncateDecimals(quoziente, this.statoMacchina.numberOfDecimals); //valore troncato
            const tmp = result.times(float_M);
            
            const R = float_A.sub(tmp);

            this.storeInRegister(R, 'R');
            this.storeInRegister(result, 'A');
        }
        if (instruction.operator == 'sqrt') {
            this._move_register_to_M(instruction);

            const float_M = this.statoMacchina.registers.reg_M.abs();	   
            const R = float_M.sqrt();
            const A = this.truncateDecimals(R, this.statoMacchina.numberOfDecimals); //valore troncato
            const M = A.times(2);
            this.statoMacchina.registers.reg_R = null;
            this.storeInRegister(A, 'A');
            this.storeInRegister(M, 'M');
        }
        if (instruction.operator == 'exchange'){
            const c = instruction.register != '/' && instruction.register!='R' && instruction.register!='A' && !this.statoMacchina.registers.reg_M.eq(0) && !this.statoMacchina.resetM ? this.statoMacchina.registers.reg_M : undefined;
            this.printInstruction(instruction, c);    
            if (instruction.register == null || instruction.register == ''){
                instruction.register = 'M';
            }
            if(instruction.register == '/'){		
                const A=this.statoMacchina.registers.reg_A;
                const dec_part_str=(A+"").split(".")[1];
                const dec_part = dec_part_str==null ? new Big(0) : new Big ('0.'+dec_part_str);
                this.storeInRegister(dec_part, 'M');
                this.statoMacchina.resetM = true;
                return;
            }
            if(instruction.register=='R'){
                console.log("moving R to A");
                const R = this.statoMacchina.registers.reg_R || new Big(0);
                this.storeInRegister(R, 'A');
                this.statoMacchina.resetM = true;
                return;
            }
            
            if(instruction.register=='A'){
                console.log("absolute value of A");
                const A = this.statoMacchina.registers.reg_A;
                const abs_A = A.abs();
                this.statoMacchina.registers.reg_A = abs_A;
                this.statoMacchina.resetM = true;
                return;
            }
            
            console.log("destination selected: ", instruction.register);
            const destination = instruction.register;
            const dst = 'reg_' + destination;
            const A = this.statoMacchina.registers.reg_A;	
            const X = this.statoMacchina.registers[dst] || new Big(0);

            this.storeInRegister(A,destination);
            this.storeInRegister(X, 'A');
        }

        if (instruction.operator == '*'){
            let constant: Big | undefined = undefined;
            const destination = instruction.register || 'M';
            
            if (destination == 'M'){
                instruction.register = '';
            }
            else if (destination != 'R') {
                const dst = "reg_" + destination;
                const value = this.statoMacchina.registers[dst] || new Big(0);
                constant = value;
                // A cannot contain null value, assign 0 instead
                const new_value = destination == 'A' ? new Big(0) : null;
                this.statoMacchina.registers[dst] = new_value;
            }
            // TODO: what to print when selected register is M or R
            this.printInstruction(instruction, constant);
        }
        if (isJumpInstruction(instruction)) {
            console.log('found a jump: ' + instruction.register + instruction.operator);
            
            const label = instruction.operator;
            if (instruction.register == 'A' || instruction.register == 'a' || instruction.register == 'B' || instruction.register == 'b'
                || instruction.register == 'E' || instruction.register == 'e' || instruction.register == 'F' || instruction.register == 'f'){
                return;
            }
            const jumpLabel = instruction.register;
            const dst = this.mapJumpLabel(jumpLabel);

            if (!isHalfRegister(dst)){
                // unconditional jump
                console.log('found an unconditional jump');
                this.seek(dst, label);
                return;
            }
            else{
                console.log('found conditional jump');
                if (this.statoMacchina.registers.reg_A.gt(0)){ //check the condition
                    console.log('type 1 jump from ' + jumpLabel);
                    this.seek(dst, label); //jump to destination
                    return;
                }
                else{
                    console.log('unsatisfied jump condition');
                }
            }
        }
        this.statoMacchina.selectedRegister = '';
        
        this.statoMacchina.resetM = true;
        this.statoMacchina.slashPressed = false;
        this.statoMacchina.signM=1;
    }
    
    private _check_registers_capacity(float_A: Big, float_M: Big) {
        if(Math.max(this.int_num(float_A),this.int_num(float_M))+Math.max(this.dec_num(float_A),this.dec_num(float_M)) > 22 ) {
            this.statoMacchina.blocked=true;
            this.statoMacchina.running = false;
            this._error = "questa operazione eccede la capienza dei registri";
            return false;
        }
        if ( this.statoMacchina.numberOfDecimals > 22 - Math.max(this.int_num(float_A),this.int_num(float_M)) ) {
            this.statoMacchina.blocked=true;
            this.statoMacchina.running = false;
            this._error = "troppi decimali richiesti";
            return false;
        }
        return true;
    }

    /**
     * 
     * perform the common operations to be done before every arithmetic instruction <register, operand>
     * 
     * - if the machine is in manual use mode, print the instruction;
     * - move the register value to M 
     * 
     * @param instruction that will be later executed using the value of M
     * 
     */
    private _move_register_to_M (instruction: Instruction) {
        this.print_instruction_and_value_of_M(instruction);

        if (instruction.register == null || instruction.register == '') {
            instruction.register = 'M';
        }
        if (instruction.register != 'M') {
            const source = instruction.register;
            console.log("moving values from ", source, " to M");
            const src = 'reg_' + source;
            const value = this.statoMacchina.registers[src] || new Big(0);
            this.storeInRegister(value, 'M');
        }
    }

    private print_instruction_and_value_of_M(instruction: Instruction) {
        const c = !this.statoMacchina.registers.reg_M.eq(0) && !this.statoMacchina.resetM ? this.statoMacchina.registers.reg_M : undefined;
        this.printInstruction(instruction, c);
    }

    private seek(dst: Register, label: Operator) {
        //Aggiunto per evitare che segnali errore di salto mentre parliamo di riferimenti...
        if (this.statoMacchina.selectedRegister == 'A' || this.statoMacchina.selectedRegister == 'a' || this.statoMacchina.selectedRegister == 'B' || this.statoMacchina.selectedRegister == 'b'
            || this.statoMacchina.selectedRegister == 'E' || this.statoMacchina.selectedRegister == 'e' || this.statoMacchina.selectedRegister == 'F' || this.statoMacchina.selectedRegister == 'f'){
            return;
        }
        console.log('searching for ' + dst + ' '+ label);
        for (let i = 0; i < this.statoMacchina.instructionCounter; i++) {
            const instruction = this.memory[i];
            if (instruction) {
                if (instruction.operator == label && instruction.register == dst ){
                    console.log('label found at ' + i);
                    this.statoMacchina.currentInstruction = i;
                    return;
                }
            }
        }
        console.log('label not found');
        this.statoMacchina.blocked = true;
        if(isHalfRegister(dst) && this.statoMacchina.currentInstruction == 0){
            this._error = 'Non puoi avviare un programma con una istruzione di salto condizionato.';
        }
        else{
            this._error = 'Riferimento non trovato.';
        }
    }

    protected runProgramBatched ( batch_size?: number ) {
        if (batch_size == null) batch_size = 0;
        let executed_instructions = 0;
        console.log('RunProgramBatched - entering the main loop');
        do {
            ++executed_instructions;
            const i = this.statoMacchina.currentInstruction || 0;
            const instruction = this.memory[i];
            if (instruction) {
                this.execute(instruction);
                this.statoMacchina.currentInstruction = (this.statoMacchina.currentInstruction || 0)+1;
            }
            else{
                this.statoMacchina.running = false;
                this.needs_to_be_started = false;
                this.statoMacchina.currentInstruction = null;
            }
        } while (this.statoMacchina.running && (batch_size<=0 || executed_instructions<batch_size));
        console.log('RunProgramBatched - SONO FERMO A ISTRUZIONE =======>', this.statoMacchina.currentInstruction);
        console.log('RunProgramBatched - ho eseguito', executed_instructions, "istruzioni");
        return this.statoMacchina.running;
    }

    protected runStepFromLabel (l: Label) {
        this.seek('A', l);
        if (!this.statoMacchina.blocked) {
            this.runStep ();
        }
    }

    protected runStep () {
        console.log('running one step of the program');
        this.statoMacchina.running = true;
        const i = this.statoMacchina.currentInstruction || 0;
        console.log('executing instruction ' + i);
        const instruction = this.memory[i];
        if (instruction) {
            this.execute(instruction);
            this.statoMacchina.currentInstruction = (this.statoMacchina.currentInstruction || 0) + 1;
        }
        else{
            this.statoMacchina.currentInstruction = null;
            this.statoMacchina.running = false;
        }
        console.log('SONO FERMO A ISTRUZIONE =======>'+this.statoMacchina.currentInstruction);
    }

    protected cambiaDecimali (n: number) {
        this.statoMacchina.numberOfDecimals = n;
    }

}
