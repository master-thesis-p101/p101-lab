/// <reference lib="webworker" />

import {P101Simulator} from "./models";
import {createMessageForService, deserializaMachineStatus, MessageFromServiceToSimulatorWorker, MessageFromSimulatorWorkerToService} from './models-messages';
import {UnicasSimulator} from './unicas_code/UnicasSimulator';

const _BATCH_INSTRUCTION_NUMBER = 500;

addEventListener('message', ( m: {data: MessageFromServiceToSimulatorWorker} ) => {
  const s: P101Simulator = new UnicasSimulator ();
  const initial_machine_status = deserializaMachineStatus (m.data.status);
  s.setInternalStatus (initial_machine_status, null, m.data.program);
  while (s.running) {
    console.group ("Simulator Worker: runProgram");
    s.runProgram (_BATCH_INSTRUCTION_NUMBER);
    console.groupEnd ();
    const m = createMessageForService ( s );
    postMessage (m);
  }
});
